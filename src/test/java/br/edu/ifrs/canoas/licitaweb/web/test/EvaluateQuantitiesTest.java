package br.edu.ifrs.canoas.licitaweb.web.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;

import br.edu.ifrs.canoas.licitaweb.web.config.MyFluentTest;
import br.edu.ifrs.canoas.licitaweb.web.page.EvaluateQuantitiesPage;
import br.edu.ifrs.canoas.licitaweb.web.page.NewBasicProjectPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EvaluateQuantitiesTest extends MyFluentTest {

	@Page
	private EvaluateQuantitiesPage evaluateQuantitiesPage;

	@Page
	private NewBasicProjectPage newBasicProjectPage;

	// TODO nicolas.w testing requirement condition
	@Test
	public void test0_checkSearchFilters() {
		// Given
		loginUser();
		evaluateQuantitiesPage.go(port);
		evaluateQuantitiesPage.isAt();
		evaluateQuantitiesPage.fillSearchFilters("ObJetÓ de Têstes 11", LocalDate.of(2018, 06, 02),
				LocalDate.of(2018, 06, 03));

		// When
		evaluateQuantitiesPage.submitSearchFilters();

		// Then
		List<FluentWebElement> trs = evaluateQuantitiesPage.getTable().find(By.xpath("//tbody//tr"));
		assertThat(trs.size()).isEqualTo(1);
		assertThat(trs.get(0).find(By.xpath("//td")).get(3).getElement().getText()).isEqualTo("Objeto de Testes 11");
	}

	// TODO nicolas.w CdT002 - RNG014, RNG022
	@Test
	public void test1_approveItem() {
		// Given
		loginUser();
		evaluateQuantitiesPage.go(port);
		evaluateQuantitiesPage.isAt();
		evaluateQuantitiesPage.goToBasicProjectPage();
		int itemIndex = 0;

		// When
		evaluateQuantitiesPage.clickApproveItemBtn(itemIndex);
		await().until(evaluateQuantitiesPage.getItem(itemIndex)).not().present();

		// Then
		FluentWebElement item = evaluateQuantitiesPage.getRefreshedItemsTable().get(itemIndex);
		assertThat(item.el("#item-status").getElement().getText()).isEqualTo("Aprovado");
		assertThat(item.el(".requested-quantity").getElement().getText())
				.isEqualTo(item.el(".approved-quantity").getElement().getText());
	}

	// TODO nicolas.w CdT002 - RNG014
	@Test
	public void test2_disapproveItem() {
		// Given
		loginUser();
		evaluateQuantitiesPage.go(port);
		evaluateQuantitiesPage.isAt();
		evaluateQuantitiesPage.goToBasicProjectPage();
		int itemIndex = 0;

		// When
		evaluateQuantitiesPage.clickDispproveItemBtn(itemIndex);
		await().until(evaluateQuantitiesPage.getItem(itemIndex)).not().present();

		// Then
		FluentWebElement item = evaluateQuantitiesPage.getRefreshedItemsTable().get(itemIndex);
		assertThat(item.el("#item-status").getElement().getText()).isEqualTo("Reprovado");
	}

	// TODO nicolas.w CdT002 - RNG017, RNG018
	@Test
	public void test3_editQuantityItem() {
		// Given
		loginUser();
		evaluateQuantitiesPage.go(port);
		evaluateQuantitiesPage.isAt();
		evaluateQuantitiesPage.goToBasicProjectPage();
		int itemIndex = 0;

		// When
		FluentWebElement inputQuantity = evaluateQuantitiesPage.getItem(itemIndex).el(".input-quantity");
		evaluateQuantitiesPage.clickEditItemBtn(itemIndex);
		await().until(inputQuantity).displayed();
		inputQuantity.fill().with("100");
		evaluateQuantitiesPage.clickConfirmEditItemBtn(itemIndex);
		await().until(evaluateQuantitiesPage.getItem(itemIndex)).not().present();

		// Then
		FluentWebElement item = evaluateQuantitiesPage.getRefreshedItemsTable().get(itemIndex);
		assertThat(item.el("#item-status").getElement().getText()).isEqualTo("N/A");
		assertThat(item.el(".input-quantity").displayed()).isFalse();
		assertThat(item.el(".approved-quantity").displayed()).isTrue();
		assertThat(item.el(".approved-quantity").getElement().getText()).isEqualTo("100.0");
	}

	// TODO nicolas.w CdT002 - RNG020 - RNG015
	@Test
	public void test4_dontReleaseToFinancialHeading() {
		// Given
		loginUser();
		evaluateQuantitiesPage.go(port);
		evaluateQuantitiesPage.isAt();
		evaluateQuantitiesPage.goToBasicProjectPage();

		evaluateQuantitiesPage.clickEditItemBtn(0);
		evaluateQuantitiesPage.clickConfirmEditItemBtn(0);
		for (int i = 1; i < evaluateQuantitiesPage.getItemsTable().size(); i++) {
			FluentWebElement elem = evaluateQuantitiesPage.getItem(i).scrollToCenter().el("#btn-approve");
			elem.getElement().click();
			await().until(elem).not().present();
		}

		// When
		assertThat(evaluateQuantitiesPage.getReleaseConfirmationModal().displayed()).isFalse();
		evaluateQuantitiesPage.getReleaseToFinancialHeadingBtn().click();
		await().atMost(5000, TimeUnit.MILLISECONDS).until(evaluateQuantitiesPage.getReleaseConfirmationModal())
				.displayed();
		assertThat(evaluateQuantitiesPage.getReleaseConfirmationModal().displayed()).isTrue();
		evaluateQuantitiesPage.getReleaseConfirmBtn().click();
		await().atMost(5000, TimeUnit.MILLISECONDS).until(evaluateQuantitiesPage.getReleaseErrorMessage()).displayed();

		// Then
		assertThat(evaluateQuantitiesPage.getReleaseErrorMessage().getElement().getText())
				.isEqualTo("Todos os itens devem ser avaliados");
		assertThat(evaluateQuantitiesPage.getBasicProjectStatus().getElement().getText())
				.isEqualTo("Aguardando avaliação das quantidades pela chefia geral dos almoxarifados");
		FluentWebElement item = evaluateQuantitiesPage.getRefreshedItemsTable().first();
		assertThat(item.getElement().getAttribute("class").contains("item-error")).isTrue();
	}

	// TODO nicolas.w CdT002 - RNG016, RNG015
	@Test
	public void test5_releaseToFinancialHeading() {
		// Given
		loginUser();
		evaluateQuantitiesPage.go(port);
		evaluateQuantitiesPage.isAt();
		evaluateQuantitiesPage.goToBasicProjectPage();
		for (int i = 0; i < evaluateQuantitiesPage.getItemsTable().size(); i++) {
			FluentWebElement item = evaluateQuantitiesPage.getItem(i).scrollToCenter();
			if (!item.el("#item-status").getElement().getText().equals("Aprovado")) {
				item.el("#btn-approve").click();
				await().atMost(1000).until(item).not().present();
			}
		}

		// When
		assertThat(evaluateQuantitiesPage.getReleaseConfirmationModal().displayed()).isFalse();
		evaluateQuantitiesPage.getReleaseToFinancialHeadingBtn().scrollToCenter().click();
		await().atMost(5000, TimeUnit.MILLISECONDS).until(evaluateQuantitiesPage.getReleaseConfirmationModal())
				.displayed();
		assertThat(evaluateQuantitiesPage.getReleaseConfirmationModal().displayed()).isTrue();
		evaluateQuantitiesPage.getReleaseConfirmBtn().click();
		await().atMost(5000, TimeUnit.MILLISECONDS).until(evaluateQuantitiesPage.getReleaseSuccessMessage())
				.displayed();

		// Then
		assertThat(evaluateQuantitiesPage.getBasicProjectStatus().getElement().getText())
				.isEqualTo("Aguardando rubrica financeira");
		assertThat(evaluateQuantitiesPage.getReleaseSuccessMessage().getElement().getText())
				.isEqualTo("Projeto básico liberado com sucesso para avaliação da rubrica financeira.");
	}

	// TODO nicolas.w CdT002 - RNG021, RNG015
	@Test
	public void test6_cancelBasicProject() {
		// Given
		loginUser();
		evaluateQuantitiesPage.go(port);
		evaluateQuantitiesPage.isAt();
		evaluateQuantitiesPage.goToOtherBasicProjectPage();
		for (int i = 0; i < evaluateQuantitiesPage.getItemsTable().size(); i++) {
			FluentWebElement item = evaluateQuantitiesPage.getItem(i).scrollToCenter();
			if (!item.el("#item-status").getElement().getText().equals("Reprovado")) {
				item.el("#btn-disapprove").click();
				await().atMost(1000).until(item).not().present();
			}
		}

		// When
		assertThat(evaluateQuantitiesPage.getReleaseConfirmationModal().displayed()).isFalse();
		evaluateQuantitiesPage.getReleaseToFinancialHeadingBtn().scrollToCenter().click();
		await().atMost(5000, TimeUnit.MILLISECONDS).until(evaluateQuantitiesPage.getReleaseConfirmationModal())
				.displayed();
		assertThat(evaluateQuantitiesPage.getReleaseConfirmationModal().displayed()).isTrue();
		evaluateQuantitiesPage.getReleaseConfirmBtn().click();
		await().atMost(5000, TimeUnit.MILLISECONDS).until(evaluateQuantitiesPage.getReleaseSuccessMessage())
				.displayed();

		// Then
		assertThat(evaluateQuantitiesPage.getBasicProjectStatus().getElement().getText()).isEqualTo("Cancelado");
		assertThat(evaluateQuantitiesPage.getReleaseSuccessMessage().getElement().getText())
				.isEqualTo("Projeto básico cancelado com sucesso pois todos os itens foram reprovados.");
	}

	// TODO nicolas.w req nao funcional - translation to English test
	@Test
	public void test7_languange_en() {
		// Given
		loginUser();
		evaluateQuantitiesPage.go(port);
		evaluateQuantitiesPage.isAt();

		// When
		evaluateQuantitiesPage.getSystemOptions().click();
		evaluateQuantitiesPage.el(By.cssSelector(".en_US")).click();

		// Then
		assertThat(window().title()).isEqualTo("Evaluate Quantities - LicitaWeb");
		assertThat(evaluateQuantitiesPage.el(By.cssSelector(".card-header>span")).getElement().getText())
				.isEqualTo("Basic Projects - Evaluate Quantities");
		assertThat(evaluateQuantitiesPage.getSubmitSearchFilters().getElement().getText()).isEqualTo("Search");
	}
}
