package br.edu.ifrs.canoas.licitaweb.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.domain.SighProduct;
import br.edu.ifrs.canoas.licitaweb.domain.Unity;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectItemService;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectService;
import br.edu.ifrs.canoas.licitaweb.service.SighProductService;
import br.edu.ifrs.canoas.licitaweb.service.UnityService;

@WebMvcTest(BasicProjectController.class)
@EnableSpringDataWebSupport
public class BasicProjectControllerTest extends BaseControllerTest {

	@MockBean
	private Messages messages;
	@MockBean
	private BasicProjectService basicProjectService;
	@MockBean
	private BasicProjectItemService basicProjectItemService;
	@MockBean
	private UnityService unityService;
	@MockBean
	private SighProductService sighProductService;

	private BasicProject testObject() {
		BasicProject basicProject = new BasicProject();
		basicProject.setId(1L);
		basicProject.setObject("Test controller object");
		basicProject.setJustification("Teste justify");
		Unity  unity = new Unity();
		unity.setId(1);
		unity.setName("HMGV");
		basicProject.setUnity(unity);
		basicProject.setSolicitationType(BasicProjectType.PRODUCTS);
		return basicProject;
	}

	private BasicProjectItem testItem() {
		BasicProjectItem item = new BasicProjectItem();
		item.setId(100L);
		item.setItemNumber(100);
		item.setName("item name");
		item.setDescription("item description");
		item.setQuantity(10.0);
		return item;
	}

	// TODO RNG011 nicolas
	@Test
	public void view_list_page() throws Exception {
		when(basicProjectService.findAllByUser(any(), any(), any()))
				.thenReturn(new PageImpl<>(new ArrayList<>()));
		this.mvc.perform(get("/basic-project").with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(status().isOk()).andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/basic-project/list"));
	}

	// TODO nicolas
	@Test
	public void delete_basic_project() throws Exception {
		when(basicProjectService.findAllByUser(any(), any(), any()))
				.thenReturn(new PageImpl<>(new ArrayList<>()));
		Long id = 1L;
		when(basicProjectService.deleteById(id)).thenReturn(Boolean.TRUE);
		this.mvc.perform(post("/basic-project/delete/" + id).with(user(userDetails)).with(csrf())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)).andExpect(status().isOk())
				.andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/basic-project/list :: paginatedList"));

		when(basicProjectService.deleteById(id)).thenReturn(Boolean.FALSE);
		this.mvc.perform(post("/basic-project/delete/" + id).with(user(userDetails)).with(csrf())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)).andExpect(status().isOk())
				.andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/basic-project/list :: paginatedList"));
	}

	// TODO nicolas
	@Test
	public void view_form_page() throws Exception {
		when(unityService.findAll()).thenReturn(new ArrayList<>());
		this.mvc.perform(get("/basic-project/new").with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(status().isOk()).andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/basic-project/form"));
	}

	// TODO RNG011 nicolas
	@Test
	public void view_form_edit_page() throws Exception {
		Long id = 1L;
		when(unityService.findAll()).thenReturn(new ArrayList<>());
		when(basicProjectService.findByIdAndRequester(id, user)).thenReturn(Optional.of(new BasicProject()));

		//WHEN FOUND THE OBJECT
		this.mvc.perform(get("/basic-project/" + id).with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(status().isOk()).andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/basic-project/form"));

		//WHEN NOT FOUND THE OBJECT
		when(basicProjectService.findByIdAndRequester(id, user)).thenReturn(Optional.empty());
		this.mvc.perform(get("/basic-project/" + id).with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(view().name("redirect:/basic-project/new"));
	}

	// TODO RNG008 nicolas
	@Test
	public void save_basic_project() throws Exception {
		BasicProject basicProject = testObject();
		when(messages.get("basicProject.saved")).thenReturn("basicProject.saved");
		when(basicProjectService.saveBasicProject(any(), any(), any())).thenReturn(1L);

		this.mvc.perform(post("/basic-project/save").with(user(userDetails)).with(csrf())
				.param("id", basicProject.getId().toString())
				.param("object", basicProject.getObject())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
				.andExpect(view().name("redirect:/basic-project/" + basicProject.getId()))
				.andExpect(flash().attribute("message", "basicProject.saved"));
	}

	// TODO RNG008 nicolas
	@Test
	public void save_basic_project_errors() throws Exception {
		BasicProject basicProject = testObject();
		when(basicProjectService.saveBasicProject(any(), any(), any())).thenReturn(null);

		this.mvc.perform(post("/basic-project/save").with(user(userDetails)).with(csrf())
				.param("id", basicProject.getId().toString())
				.param("object", basicProject.getObject())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
		.andExpect(view().name("/basic-project/form"));
	}

	// TODO RNG006 / RNG007 nicolas
	@Test
	public void release_basic_project() throws Exception {
		BasicProject basicProject = testObject();
		when(messages.get("basicProject.released")).thenReturn("basicProject.released");
		when(mock(BindingResult.class).hasErrors()).thenReturn(false);
		when(basicProjectService.releaseForEvaluation(any(), any(), any())).thenReturn(basicProject);

		this.mvc.perform(post("/basic-project/save").with(user(userDetails)).with(csrf())
				.param("action", "release")
				.param("id", basicProject.getId().toString())
				.param("object", basicProject.getObject())
				.param("justification", basicProject.getJustification())
				.param("unity.id", basicProject.getUnity().getId().toString())
				.param("solicitationType", basicProject.getSolicitationType().toString())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
				.andExpect(view().name("redirect:/basic-project/" + basicProject.getId()))
				.andExpect(flash().attribute("message", "basicProject.released"));
	}

	// TODO RNG006 / RNG007 nicolas
	@Test
	public void release_basic_project_errors() throws Exception {
		BasicProject basicProject = testObject();
		when(basicProjectService.releaseForEvaluation(any(), any(), any())).thenReturn(basicProject);

		this.mvc.perform(post("/basic-project/save").with(user(userDetails)).with(csrf())
				.param("action", "release")
				.param("id", basicProject.getId().toString())
				.param("object", basicProject.getObject())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
		.andExpect(view().name("/basic-project/form"));
	}

	// TODO RNG003 nicolas
	@Test
	public void view_new_item() throws Exception {
		BasicProject basicProject = testObject();
		when(basicProjectService.findById(any())).thenReturn(Optional.of(basicProject));
		when(basicProjectService.findByIdAndRequester(any(), any())).thenReturn(Optional.of(basicProject));

		this.mvc.perform(get("/basic-project/" + basicProject.getId() + "/item/new")
				.with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(status().isOk())
				.andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/basic-project/form :: itemModalFragment"));
	}

	// TODO RNG003 nicolas
	@Test
	public void view_edit_item() throws Exception {
		BasicProject basicProject = testObject();
		when(basicProjectService.findById(any())).thenReturn(Optional.of(basicProject));
		when(basicProjectService.findByIdAndRequester(any(), any())).thenReturn(Optional.of(basicProject));

		BasicProjectItem item = testItem();
		item.setBasicProject(basicProject);
		when(basicProjectItemService.findById(any())).thenReturn(Optional.of(item));
		when(sighProductService.findById(any())).thenReturn(new SighProduct());

		this.mvc.perform(get("/basic-project/" + basicProject.getId() + "/item/" + item.getId())
				.with(user(userDetails)).accept(MediaType.TEXT_HTML))
		.andExpect(status().isOk())
		.andExpect(content().contentType("text/html;charset=UTF-8"))
		.andExpect(view().name("/basic-project/form :: itemModalFragment"));
	}

	// TODO RNG003 nicolas
	@Test
	public void view_edit_item_whenTypeServices() throws Exception {
		BasicProject basicProject = testObject();
		basicProject.setSolicitationType(BasicProjectType.SERVICES);
		when(basicProjectService.findById(any())).thenReturn(Optional.of(basicProject));
		when(basicProjectService.findByIdAndRequester(any(), any())).thenReturn(Optional.of(basicProject));

		BasicProjectItem item = testItem();
		item.setBasicProject(basicProject);
		when(basicProjectItemService.findById(any())).thenReturn(Optional.of(item));
		when(sighProductService.findById(any())).thenReturn(new SighProduct());

		this.mvc.perform(get("/basic-project/" + basicProject.getId() + "/item/" + item.getId())
				.with(user(userDetails)).accept(MediaType.TEXT_HTML))
		.andExpect(status().isOk())
		.andExpect(content().contentType("text/html;charset=UTF-8"))
		.andExpect(view().name("/basic-project/form :: itemModalFragment"));
	}

	// TODO RNG003 nicolas
	@Test
	public void view_edit_item_whenNullItemNumber() throws Exception {
		BasicProject basicProject = testObject();
		BasicProjectItem item = testItem();
		item.setBasicProject(basicProject);
		item.setItemNumber(null);
		when(basicProjectService.findById(any())).thenReturn(Optional.of(basicProject));
		when(basicProjectService.findByIdAndRequester(any(), any())).thenReturn(Optional.of(basicProject));
		when(basicProjectItemService.findById(any())).thenReturn(Optional.of(item));
		when(sighProductService.findById(any())).thenReturn(new SighProduct());

		this.mvc.perform(get("/basic-project/" + basicProject.getId() + "/item/" + item.getId())
				.with(user(userDetails)).accept(MediaType.TEXT_HTML))
		.andExpect(status().isOk())
		.andExpect(content().contentType("text/html;charset=UTF-8"))
		.andExpect(view().name("/basic-project/form :: itemModalFragment"));
	}

	// TODO RNG004 nicolas
	@Test
	public void save_new_item() throws Exception {
		BasicProject basicProject = testObject();
		BasicProjectItem item = testItem();
		item.setId(null);
		item.setBasicProject(basicProject);
		when(basicProjectService.findById(any())).thenReturn(Optional.of(basicProject));
		when(basicProjectService.findByIdAndRequester(any(), any())).thenReturn(Optional.of(basicProject));
		when(basicProjectItemService.findById(any())).thenReturn(Optional.of(item));
		when(messages.get("bpItem.addedSuccessful")).thenReturn("added");
		when(basicProjectItemService.addOrUpdateItem(any(), any(), any())).thenReturn(item);

		this.mvc.perform(post("/basic-project/" + basicProject.getId() + "/item/save").with(user(userDetails)).with(csrf())
				.param("itemNumber", item.getItemNumber().toString())
				.param("name", item.getName())
				.param("description", item.getDescription())
				.param("quantity", item.getQuantity().toString())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
		.andExpect(view().name("/basic-project/form :: itemModalFragment"))
		.andExpect(model().attribute("modalMessage", "added"));
	}

	// TODO nicolas
	@Test
	public void update_item() throws Exception {
		BasicProject basicProject = testObject();
		BasicProjectItem item = testItem();
		item.setBasicProject(basicProject);
		when(basicProjectService.findById(any())).thenReturn(Optional.of(basicProject));
		when(basicProjectService.findByIdAndRequester(any(), any())).thenReturn(Optional.of(basicProject));
		when(basicProjectItemService.findById(any())).thenReturn(Optional.of(item));
		when(sighProductService.findById(any())).thenReturn(new SighProduct());
		when(messages.get("bpItem.editedSuccessful")).thenReturn("edited");
		when(basicProjectItemService.addOrUpdateItem(any(), any(), any())).thenReturn(item);

		this.mvc.perform(post("/basic-project/" + basicProject.getId() + "/item/save").with(user(userDetails)).with(csrf())
				.param("id", item.getId().toString())
				.param("itemNumber", item.getItemNumber().toString())
				.param("name", item.getName())
				.param("description", item.getDescription())
				.param("quantity", item.getQuantity().toString())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
				.andExpect(view().name("/basic-project/form :: itemModalFragment"))
				.andExpect(model().attribute("modalMessage", "edited"));
	}

	// TODO nicolas
	@Test
	public void save_or_update_new_item_errors() throws Exception {
		BasicProject basicProject = testObject();
		BasicProjectItem item = testItem();
		item.setId(null);
		item.setName(null);
		item.setDescription(null);
		item.setQuantity(null);
		item.setBasicProject(basicProject);
		when(basicProjectService.findById(any())).thenReturn(Optional.of(basicProject));
		when(basicProjectService.findByIdAndRequester(any(), any())).thenReturn(Optional.of(basicProject));
		when(basicProjectItemService.findById(any())).thenReturn(Optional.of(item));
		when(messages.get("bpItem.addedSuccessful")).thenReturn("added");
		when(basicProjectItemService.addOrUpdateItem(any(), any(), any())).thenReturn(item);

		this.mvc.perform(post("/basic-project/" + basicProject.getId() + "/item/save").with(user(userDetails)).with(csrf())
				.param("itemNumber", item.getItemNumber().toString())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
		.andExpect(view().name("/basic-project/form :: itemModalFragment"))
		.andExpect(model().attributeErrorCount("basicProjectItem", 3));
	}

	// TODO nicolas
	@Test
	public void delete_item() throws Exception {
		BasicProject basicProject = testObject();
		this.mvc.perform(post("/basic-project/" + basicProject.getId() + "/item/delete-items").with(user(userDetails)).with(csrf())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
		.andExpect(view().name("redirect:/basic-project/" + basicProject.getId()));
	}

	// TODO RNG002 nicolas
	@Test
	public void view_refresh_items_table() throws Exception {
		BasicProject basicProject = testObject();
		when(unityService.findAll()).thenReturn(new ArrayList<>());
		when(basicProjectService.findByIdAndRequester(any(), any())).thenReturn(Optional.of(new BasicProject()));
		this.mvc.perform(get("/basic-project/" + basicProject.getId() + "/refresh-items-table").with(user(userDetails)).with(csrf())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
		.andExpect(view().name("/basic-project/form :: projectItemsFragment"));
	}
}