package br.edu.ifrs.canoas.licitaweb.report;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.domain.Unity;
import br.edu.ifrs.canoas.licitaweb.domain.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class BasicProjectItemsExcelViewTest {

	@Autowired
	private BasicProjectItemsExcelView excelView;

	private final MockHttpServletRequest request = new MockHttpServletRequest();

	private final MockHttpServletResponse response = new MockHttpServletResponse();

	@Test
	public void buildXlsFile() throws Exception {
		// given
		Unity unity = Unity.builder().id(1).name("HMGV").build();
		User requester = User.builder().id(1L).name("User Root").build();
		BasicProject basicProject = BasicProject.builder().id(100L).unity(unity).object("Java Rules test!").requester(requester)
				.justification("JAVA").solicitationType(BasicProjectType.PRODUCTS).releasedDateToQuantitiesEvaluation(LocalDateTime.now())
				.createdDate(LocalDateTime.now().minusDays(2)).basicProjectItems(new ArrayList<>()).build();

		for (int i = 1; i <= 5; i++) {
			BasicProjectItem item = BasicProjectItem.builder().basicProject(basicProject)
					.id(Integer.valueOf(i).longValue()).description("test " + i).name("name test " + i)
					.approved(Boolean.TRUE).build();
			basicProject.getBasicProjectItems().add(item);
		}
		for (int i = 1; i <= 5; i++) {
			BasicProjectItem item = BasicProjectItem.builder().basicProject(basicProject)
					.id(Integer.valueOf(i).longValue()).description("test " + i).name("name test " + i)
					.approved(Boolean.FALSE).build();
			basicProject.getBasicProjectItems().add(item);
		}
		for (int i = 1; i <= 5; i++) {
			BasicProjectItem item = BasicProjectItem.builder().basicProject(basicProject)
					.id(Integer.valueOf(i).longValue()).description("test " + i).name("name test " + i).approved(null)
					.build();
			basicProject.getBasicProjectItems().add(item);
		}

		// when
		ModelAndView mav = new ModelAndView(excelView, "basicProject", basicProject);
		excelView.render(mav.getModel(), request, response);

		// then
		assertThat(mav.getModel()).isNotNull();
		assertThat(mav.getView().getContentType()).isEqualTo("application/vnd.ms-excel");
	}
}
