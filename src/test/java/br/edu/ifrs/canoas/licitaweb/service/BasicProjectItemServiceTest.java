package br.edu.ifrs.canoas.licitaweb.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;
import br.edu.ifrs.canoas.licitaweb.repository.UnityRepository;
import br.edu.ifrs.canoas.licitaweb.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class BasicProjectItemServiceTest {

	@Autowired
	private EntityManager entityManager;
	@Autowired
	private BasicProjectService basicProjectService;
	@Autowired
	private BasicProjectItemService itemService;
	@Autowired
	private SighProductService sighProductService;
	@Autowired
	private UnityRepository unityRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private Messages messages;

	private final String OBJECT = "Java Rules test!";
	private final String JUSTIFICATION = "JAVA";
	private final BasicProjectType TYPE = BasicProjectType.PRODUCTS;
	private final ProjectStatus STATUS = ProjectStatus.QUANTITY_EVALUATION;
	private final Integer UNITY_ID = 100;
	private final Long USER_ID = 1L;
	private final Integer NUMBER_OF_ITEMS = 10;
	private final Integer PRODUCT_ID = 1;
	private final Integer ITEM_NUMBER = 100;
	private final String ITEM_NAME = "Item Test Name";
	private final String ITEM_DESCRIPTION = "Item Test Description";
	private final Double ITEM_QUANTITY = 10.0;

	private BasicProject loadTestBasicProject() {
		BasicProject basicProject = new BasicProject();
		basicProject.setObject(OBJECT);
		basicProject.setJustification(JUSTIFICATION);
		basicProject.setSolicitationType(TYPE);
		basicProject.setStatus(STATUS);
		basicProject.setUnity(unityRepository.findOne(UNITY_ID));
		basicProject.setRequester(userRepository.findOne(USER_ID));
		return basicProject;
	}

	private Long loadTestBasicProjectInDatabase() {
		BasicProject basicProject = loadTestBasicProject();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		return basicProjectService.saveBasicProject(bindingResult, basicProject.getRequester(), basicProject);
	}

	private BasicProject loadTestServiceBasicProject() {
		BasicProject basicProject = new BasicProject();
		basicProject.setObject(OBJECT);
		basicProject.setJustification(JUSTIFICATION);
		basicProject.setSolicitationType(BasicProjectType.SERVICES);
		basicProject.setStatus(STATUS);
		basicProject.setUnity(unityRepository.findOne(UNITY_ID));
		basicProject.setRequester(userRepository.findOne(USER_ID));
		return basicProject;
	}

	private Long loadTestServiceBasicProjectInDatabase() {
		BasicProject basicProject = loadTestServiceBasicProject();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		return basicProjectService.saveBasicProject(bindingResult, basicProject.getRequester(), basicProject);
	}

	private BasicProject loadTestItemsInBasicProject(Long basicProjectId) {
		BasicProject basicProject = basicProjectService.findById(basicProjectId).get();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		for (int i = 1; i <= NUMBER_OF_ITEMS; i++) {
			BasicProjectItem item = new BasicProjectItem();
			item.setBasicProject(basicProject);
			item.setSighProduct(sighProductService.findById(i));
			item.setQuantity(Double.valueOf(i));
			item = itemService.addOrUpdateItem(basicProjectId, item, bindingResult);
		}
		entityManager.refresh(basicProject);
		return basicProject;
	}

	private BasicProject loadTestReleasedObjectInDatabase() {
		Long id = loadTestBasicProjectInDatabase();
		BasicProject basicProject = loadTestItemsInBasicProject(id);
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		return basicProjectService.releaseForEvaluation(bindingResult, basicProject.getRequester(), basicProject);
	}

	private BasicProjectItem loadTestProductItem() {
		BasicProjectItem item = new BasicProjectItem();
		item.setSighProduct(sighProductService.findById(PRODUCT_ID));
		item.setQuantity(ITEM_QUANTITY);
		return item;
	}

	private BasicProjectItem loadTestServiceItem() {
		BasicProjectItem item = new BasicProjectItem();
		item.setItemNumber(ITEM_NUMBER);
		item.setName(ITEM_NAME);
		item.setDescription(ITEM_DESCRIPTION);
		item.setQuantity(ITEM_QUANTITY);
		return item;
	}

	private BasicProjectItem loadTestProductItemInDatabase() {
		Long basicProjectId = loadTestBasicProjectInDatabase();
		BasicProjectItem item = loadTestProductItem();
		BindingResult bindingResult = new BeanPropertyBindingResult(item, "itemBasicProject");
		item = itemService.addOrUpdateItem(basicProjectId, item, bindingResult);
		return item;
	}

	// TODO nicolas
	@Test
	public void shouldReturnOptionalById() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();

		// when
		Optional<BasicProjectItem> optional = itemService.findById(item.getId());

		// then
		assertThat(optional.isPresent()).isTrue();
		assertThat(optional.get()).extracting("quantity").contains(10.0);
	}

	// TODO nicolas
	@Test
	public void shouldReturnEmptyByNullId() {
		// given
		Long id = null;

		// when
		Optional<BasicProjectItem> optional = itemService.findById(id);

		// then
		assertThat(optional.isPresent()).isFalse();
	}

	// TODO RNG008 / RNG009 / RNG010 nicolas
	@Test
	public void shouldAddProductItem() {
		// given
		Long basicProjectId = loadTestBasicProjectInDatabase();
		BasicProjectItem item = loadTestProductItem();
		BindingResult bindingResult = new BeanPropertyBindingResult(item, "itemBasicProject");

		// when
		item = itemService.addOrUpdateItem(basicProjectId, item, bindingResult);

		// then
		assertThat(item.getId()).isNotNull();
		assertThat(bindingResult.hasErrors()).isFalse();
	}

	// TODO RNG008 / RNG010 nicolas
	@Test
	public void shouldAddServiceItem() {
		// given
		Long basicProjectId = loadTestServiceBasicProjectInDatabase();
		BasicProjectItem item = loadTestServiceItem();
		BindingResult bindingResult = new BeanPropertyBindingResult(item, "itemBasicProject");

		// when
		item = itemService.addOrUpdateItem(basicProjectId, item, bindingResult);

		// then
		assertThat(item.getId()).isNotNull();
		assertThat(bindingResult.hasErrors()).isFalse();
	}

	// TODO RNG008 nicolas
	@Test
	public void shouldNotAddItemWhenBasicProjectReleased() {
		// given
		BasicProject basicProject = loadTestReleasedObjectInDatabase();
		BasicProjectItem item = loadTestProductItem();
		BindingResult bindingResult = new BeanPropertyBindingResult(item, "itemBasicProject");

		// when
		item = itemService.addOrUpdateItem(basicProject.getId(), item, bindingResult);

		// then
		assertThat(item.getId()).isNull();
		assertThat(bindingResult.hasErrors()).isTrue();
		assertThat(bindingResult.getGlobalError().getDefaultMessage())
				.isEqualTo(messages.get("basicProject.alreadyReleased"));
	}

	// TODO RNG009 nicolas
	@Test
	public void shouldNotAddItemProductWhenSighProductIsNull() {
		// given
		Long basicProjectId = loadTestBasicProjectInDatabase();
		BasicProjectItem item = new BasicProjectItem();
		item.setQuantity(ITEM_QUANTITY);
		BindingResult bindingResult = new BeanPropertyBindingResult(item, "itemBasicProject");

		// when
		item = itemService.addOrUpdateItem(basicProjectId, item, bindingResult);

		// then
		assertThat(item.getId()).isNull();
		assertThat(bindingResult.hasErrors()).isTrue();
		assertThat(bindingResult.getFieldError("sighProduct").getDefaultMessage())
				.isEqualTo(messages.get("field.not-null"));
	}

	// TODO RNG010 nicolas
	@Test
	public void shouldNotAddItemWithSameItemCodeTwice() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();
		BasicProjectItem item2 = new BasicProjectItem();
		item2.setQuantity(ITEM_QUANTITY);
		item2.setItemNumber(item.getItemNumber());
		BindingResult bindingResult = new BeanPropertyBindingResult(item, "itemBasicProject");

		// when
		item2 = itemService.addOrUpdateItem(item.getBasicProject().getId(), item2, bindingResult);

		// then
		assertThat(item2.getId()).isNull();
		assertThat(bindingResult.hasErrors()).isTrue();
		assertThat(bindingResult.getGlobalError().getDefaultMessage())
				.isEqualTo(messages.get("basicProject.notPossibleSameItemCodeTwice"));
	}

	// TODO RNG010 nicolas
	@Test
	public void shouldUpdateProductItem() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();
		BindingResult bindingResult = new BeanPropertyBindingResult(item, "itemBasicProject");
		item.setQuantity(ITEM_QUANTITY + 1);

		// when
		item = itemService.addOrUpdateItem(item.getBasicProject().getId(), item, bindingResult);

		// then
		assertThat(item).extracting("id").isNotNull();
		assertThat(bindingResult.hasErrors()).isFalse();
	}

	// TODO nicolas
	@Test
	public void shouldDeleteSelectedItems() {
		// given
		Long id = loadTestBasicProjectInDatabase();
		BasicProject basicProject = loadTestItemsInBasicProject(id);
		assertThat(basicProject.getBasicProjectItems()).isNotEmpty();

		// when
		basicProject.getBasicProjectItems().get(0).setSelected(Boolean.TRUE);
		basicProject.getBasicProjectItems().get(2).setSelected(Boolean.TRUE);
		basicProject.getBasicProjectItems().get(4).setSelected(Boolean.TRUE);
		itemService.deleteItems(basicProject);

		// then
		for (BasicProjectItem item : basicProject.getBasicProjectItems()) {
			if (item.isSelected())
				assertThat(itemService.findById(item.getId()).isPresent()).isFalse();
			else
				assertThat(itemService.findById(item.getId()).isPresent()).isTrue();
		}
	}

	// TODO nicolas
	@Test
	public void shouldNotDeleteWhenNull() {
		// given
		BasicProject basicProject = null;
		BasicProject basicProject2 = new BasicProject();
		basicProject2.setBasicProjectItems(new ArrayList<>());

		// when
		itemService.deleteItems(basicProject);
		itemService.deleteItems(basicProject2);

		// then
		assertThat(basicProject).isNull();
		assertThat(basicProject2.getBasicProjectItems()).isEmpty();
	}

	@Test
	public void shouldChangeQuantity() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();
		item.setQuantityApproved(ITEM_QUANTITY - 1);

		// when
		item = itemService.changeQuantity(item);

		// then
		assertThat(item).extracting("id").isNotNull();
		assertThat(item.getQuantityApproved()).isEqualTo(ITEM_QUANTITY - 1);
		assertThat(item.getApproved()).isNull();
	}

	@Test
	public void shouldApproveItem() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();

		// when
		item = itemService.approveItem(item.getId());

		// then
		assertThat(item).extracting("id").isNotNull();
		assertThat(item.getQuantityApproved()).isNotNull();
		assertThat(item.getApproved()).isTrue();
	}

	@Test
	public void shouldApproveItemWithChangedQuantity() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();
		item.setQuantityApproved(ITEM_QUANTITY - 1);

		// when
		item = itemService.changeQuantity(item);
		item = itemService.approveItem(item.getId());

		// then
		assertThat(item).extracting("id").isNotNull();
		assertThat(item.getQuantityApproved()).isEqualTo(ITEM_QUANTITY - 1);
		assertThat(item.getApproved()).isTrue();
	}

	@Test
	public void shouldDisapproveItem() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();

		// when
		item = itemService.disapproveItem(item.getId());

		// then
		assertThat(item).extracting("id").isNotNull();
		assertThat(item.getApproved()).isFalse();
	}

	@Test
	public void shouldNotChangeQuantityOfNewItem() {
		// given
		BasicProjectItem item = new BasicProjectItem();

		// when
		item = itemService.changeQuantity(item);

		// then
		assertThat(item).isNull();
	}

	@Test
	public void shouldNotApproveNewItem() {
		// given
		BasicProjectItem item = new BasicProjectItem();

		// when
		item = itemService.approveItem(item.getId());

		// then
		assertThat(item).isNull();
	}

	@Test
	public void shouldNotDisapproveNewItem() {
		// given
		BasicProjectItem item = new BasicProjectItem();

		// when
		item = itemService.disapproveItem(item.getId());

		// then
		assertThat(item).isNull();
	}

	@Test
	public void shouldChangeFinancialHeading() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();
		String heading = "TEST_FINANCIAL-HEADING-1";

		// when
		item.setFinancialHeading(heading);
		BasicProjectItem returnedItem = itemService.changeFinancialHeading(item);
		BasicProjectItem foundItem = itemService.findById(item.getId()).orElse(new BasicProjectItem());

		// then
		assertThat(returnedItem).extracting("id").isNotNull();
		assertThat(returnedItem.getFinancialHeading()).isEqualTo(heading);
		assertThat(foundItem.getFinancialHeading()).isEqualTo(heading);
	}

	@Test
	public void shouldChangeEmptyFinancialHeading() {
		// given
		BasicProjectItem item = loadTestProductItemInDatabase();
		String heading = " ";

		// when
		item.setFinancialHeading(heading);
		BasicProjectItem returnedItem = itemService.changeFinancialHeading(item);
		BasicProjectItem foundItem = itemService.findById(item.getId()).orElse(new BasicProjectItem());

		// then
		assertThat(returnedItem).extracting("id").isNotNull();
		assertThat(returnedItem.getFinancialHeading()).isEqualTo(null);
		assertThat(foundItem.getFinancialHeading()).isEqualTo(null);
	}

	@Test
	public void shouldNotChangeFinancialHeadingOfNullId() {
		// given
		BasicProjectItem item = new BasicProjectItem();
		String heading = " ";

		// when
		item.setFinancialHeading(heading);
		BasicProjectItem returnedItem = itemService.changeFinancialHeading(item);
		BasicProjectItem foundItem = itemService.findById(item.getId()).orElse(null);

		// then
		assertThat(returnedItem).isNull();
		assertThat(foundItem).isNull();
	}
}
