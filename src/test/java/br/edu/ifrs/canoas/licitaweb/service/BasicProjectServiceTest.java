package br.edu.ifrs.canoas.licitaweb.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;
import br.edu.ifrs.canoas.licitaweb.domain.User;
import br.edu.ifrs.canoas.licitaweb.filter.BasicProjectFilter;
import br.edu.ifrs.canoas.licitaweb.repository.BasicProjectRepository;
import br.edu.ifrs.canoas.licitaweb.repository.UnityRepository;
import br.edu.ifrs.canoas.licitaweb.repository.UserRepository;
import br.edu.ifrs.canoas.licitaweb.util.PageRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class BasicProjectServiceTest {

	@Autowired
	private EntityManager entityManager;
	@Autowired
	private Messages messages;
	@Autowired
	private BasicProjectService basicProjectService;
	@Autowired
	private BasicProjectItemService itemService;
	@Autowired
	private SighProductService sighProductService;
	@Autowired
	private BasicProjectRepository basicProjectRepository;
	@Autowired
	private UnityRepository unityRepository;
	@Autowired
	private UserRepository userRepository;

	private final String OBJECT = "Java Rules test!";
	private final String JUSTIFICATION = "JAVA";
	private final BasicProjectType TYPE = BasicProjectType.PRODUCTS;
	private final ProjectStatus STATUS = ProjectStatus.QUANTITY_EVALUATION;

	private final Integer UNITY_ID = 100;
	private final Long USER_ID = 1L;
	private final Integer NUMBER_OF_ITEMS = 10;

	private BasicProject loadTestBasicProject() {
		BasicProject basicProject = new BasicProject();
		basicProject.setObject(OBJECT);
		basicProject.setJustification(JUSTIFICATION);
		basicProject.setSolicitationType(TYPE);
		basicProject.setStatus(STATUS);
		basicProject.setUnity(unityRepository.findOne(UNITY_ID));
		basicProject.setRequester(userRepository.findOne(USER_ID));
		return basicProject;
	}

	private Long loadTestBasicProjectInDatabase() {
		BasicProject basicProject = loadTestBasicProject();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		return basicProjectService.saveBasicProject(bindingResult, basicProject.getRequester(), basicProject);
	}

	private BasicProject loadTestItemsInBasicProject(Long basicProjectId) {
		BasicProject basicProject = basicProjectService.findById(basicProjectId).get();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		for (int i = 1; i <= NUMBER_OF_ITEMS; i++) {
			BasicProjectItem item = new BasicProjectItem();
			item.setBasicProject(basicProject);
			item.setSighProduct(sighProductService.findById(i));
			item.setQuantity(Double.valueOf(i));
			item = itemService.addOrUpdateItem(basicProjectId, item, bindingResult);
		}
		entityManager.refresh(basicProject);
		return basicProject;
	}

	private BasicProject loadTestReleasedObjectInDatabase() {
		Long id = loadTestBasicProjectInDatabase();
		BasicProject basicProject = loadTestItemsInBasicProject(id);
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		return basicProjectService.releaseForEvaluation(bindingResult, basicProject.getRequester(), basicProject);
	}

	// TODO RNG012 nicolas
	@Test
	public void shouldReturnPage0ByFilterAndPageRequest0() {
		// given
		loadTestBasicProjectInDatabase();
		BasicProjectFilter filter = new BasicProjectFilter();
		filter.setObject("Rules test");
		Pageable pageable = PageRequest.of(0, 1);

		// when
		Page<BasicProject> page = basicProjectService.findAll(filter, pageable);

		// then
		assertThat(page.getNumber()).isEqualTo(0);
		assertThat(page.getContent()).isNotEmpty().extracting("object").contains("Java Rules test!");
	}

	// TODO RNG012 nicolas
	@Test
	public void shouldReturnPage0ByFilterAndPageRequest1() {
		// given
		loadTestBasicProjectInDatabase();
		BasicProjectFilter filter = new BasicProjectFilter();
		filter.setObject("JAVA Rúles test");
		Pageable pageable = PageRequest.of(1, 1);

		// when
		Page<BasicProject> page = basicProjectService.findAll(filter, pageable);

		// then
		assertThat(page.getNumber()).isEqualTo(0);
		assertThat(page.getContent()).isNotEmpty().extracting("object").contains("Java Rules test!");
	}

	// TODO RNG012 nicolas
	@Test
	public void shouldReturnPage1ByFilterAndPageRequest2WithTwoEntities() {
		// given
		loadTestBasicProjectInDatabase();
		loadTestBasicProjectInDatabase();
		BasicProjectFilter filter = new BasicProjectFilter();
		filter.setObject("Rules test");
		Pageable pageable = PageRequest.of(2, 1);

		// when
		Page<BasicProject> page = basicProjectService.findAll(filter, pageable);

		// then
		assertThat(page.getNumber()).isEqualTo(1);
		assertThat(page.getContent()).isNotEmpty().extracting("object").contains("Java Rules test!");
	}

	// TODO nicolas
	@Test
	public void shouldReturnOptionalById() {
		// given
		Long id = loadTestBasicProjectInDatabase();

		// when
		Optional<BasicProject> optional = basicProjectService.findById(id);

		// then
		assertThat(optional.isPresent()).isTrue();
		assertThat(optional.get()).extracting("object").contains("Java Rules test!");
	}

	// TODO RNG0011 nicolas
	@Test
	public void shouldReturnOptionalByIdAndRequester() {
		// given
		Long id = loadTestBasicProjectInDatabase();

		// when
		Optional<BasicProject> optional = basicProjectService.findByIdAndRequester(id,
				userRepository.findOne(USER_ID));

		// then
		assertThat(optional.isPresent()).isTrue();
		assertThat(optional.get()).extracting("object").contains("Java Rules test!");
		assertThat(optional.get()).extracting("requester.id").contains(USER_ID);
	}

	// TODO RNG0011 nicolas
	@Test
	public void shouldReturnEmptyOptionalByIdAndNullRequester() {
		// given
		Long id = loadTestBasicProjectInDatabase();

		// when
		Optional<BasicProject> optional = basicProjectService.findByIdAndRequester(id, null);

		// then
		assertThat(optional.isPresent()).isFalse();
	}

	// TODO RNG0011 nicolas
	@Test
	public void shouldReturnEmptyOptionalByIdAndNullRequesterId() {
		// given
		Long id = loadTestBasicProjectInDatabase();

		// when
		Optional<BasicProject> optional = basicProjectService.findByIdAndRequester(id, new User());

		// then
		assertThat(optional.isPresent()).isFalse();
	}

	// TODO nicolas
	@Test
	public void shouldDeleteById() {
		// given
		Long id = loadTestBasicProjectInDatabase();

		// when
		Boolean deleted = basicProjectService.deleteById(id);

		// then
		assertThat(deleted).isTrue();
	}

	// TODO nicolas
	@Test
	public void shouldNotDeleteByNullId() {
		// given
		loadTestBasicProjectInDatabase();

		// when
		Boolean deleted = basicProjectService.deleteById(null);

		// then
		assertThat(deleted).isFalse();
	}

	// TODO nicolas
	@Test
	public void shouldDeleteBasicProjectAndItemsById() {
		// given
		Long id = loadTestBasicProjectInDatabase();
		BasicProject basicProject = loadTestItemsInBasicProject(id);
		assertThat(basicProject.getBasicProjectItems()).isNotEmpty();

		// when
		Boolean deleted = basicProjectService.deleteById(id);

		// then
		assertThat(deleted).isTrue();
		for (BasicProjectItem item : basicProject.getBasicProjectItems()) {
			assertThat(itemService.findById(item.getId()).isPresent()).isFalse();
		}
	}

	// TODO nicolas
	@Test
	public void shouldSaveBasicProject() {
		// given
		BasicProject basicProject = loadTestBasicProject();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");

		// when
		Long id = basicProjectService.saveBasicProject(bindingResult, basicProject.getRequester(), basicProject);

		// then
		assertThat(id).isNotNull();
	}

	// TODO nicolas
	@Test
	public void shouldNotSaveNullBasicProject() {
		// given
		BasicProject basicProject = loadTestBasicProject();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");

		// when
		Long id = basicProjectService.saveBasicProject(bindingResult, basicProject.getRequester(), null);

		// then
		assertThat(id).isNull();
	}

	// TODO nicolas
	@Test
	public void shouldNotSaveBasicProjectWhenHasErrors() {
		// given
		BasicProject basicProject = loadTestBasicProject();
		BindingResult bindingResult = mock(BindingResult.class);
		doReturn(true).when(bindingResult).hasErrors();

		// when
		Long id = basicProjectService.saveBasicProject(bindingResult, basicProject.getRequester(), basicProject);

		// then
		assertThat(id).isNull();
	}

	// TODO RNG008 nicolas
	@Test
	public void shouldNotSaveBasicProjectWhenChangesDisabled() {
		// given
		BasicProject basicProject = loadTestReleasedObjectInDatabase();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");

		// when
		Long id = basicProjectService.saveBasicProject(bindingResult, basicProject.getRequester(), basicProject);

		// then
		assertThat(id).isNull();
	}

	// TODO RNG007 nicolas
	@Test
	public void shouldReleaseForEvaluation() {
		// given
		Long id = loadTestBasicProjectInDatabase();
		BasicProject basicProject = loadTestItemsInBasicProject(id);
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		assertThat(basicProject.getProcessNumber()).isEqualTo("/");

		// when
		basicProject = basicProjectService.releaseForEvaluation(bindingResult, basicProject.getRequester(),
				basicProject);

		// then
		assertThat(basicProject).isNotNull();
		assertThat(basicProject.getStatus()).isEqualTo(ProjectStatus.QUANTITY_EVALUATION);
		assertThat(basicProject.getProcessNumber())
				.isEqualTo(String.format("%03d", 4) + "/" + LocalDate.now().getYear());
	}

	// TODO RNG006 nicolas
	@Test
	public void shouldNotReleaseForEvaluationWhenNoItems() {
		// given
		Long id = loadTestBasicProjectInDatabase();
		BasicProject basicProject = basicProjectService.findById(id).get();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");

		// when
		basicProject = basicProjectService.releaseForEvaluation(bindingResult, basicProject.getRequester(),
				basicProject);

		// then
		assertThat(basicProject).isNotNull();
		assertThat(basicProject.getStatus()).isEqualTo(ProjectStatus.DRAFT);
	}

	@Test
	public void shouldReturnPage0ByFilterAndPageRequest1AndUser() {
		// given
		loadTestBasicProjectInDatabase();
		BasicProjectFilter filter = new BasicProjectFilter();
		filter.setObject("JAVA Rúles test");
		Pageable pageable = PageRequest.of(1, 1);
		User user = userRepository.findOne(USER_ID);

		// when
		Page<BasicProject> page = basicProjectService.findAllByUser(filter, pageable, user);

		// then
		assertThat(page.getNumber()).isEqualTo(0);
		assertThat(page.getContent()).isNotEmpty().extracting("object").contains("Java Rules test!");
	}

	@Test
	public void shouldReturnPage0ByFilterAndPageRequest1ToQuantitiesEvaluation() {
		// given
		loadTestBasicProjectInDatabase();
		loadTestReleasedObjectInDatabase();
		BasicProjectFilter filter = new BasicProjectFilter();
		filter.setObject("JAVA Rúles test");
		Pageable pageable = PageRequest.of(1, 1);

		// when
		Page<BasicProject> page = basicProjectService.findToQuantitiesEvaluation(filter, pageable);

		// then
		assertThat(page.getNumber()).isEqualTo(0);
		assertThat(page.getContent()).isNotEmpty().extracting("object").contains("Java Rules test!");
		for (BasicProject bp : page.getContent()) {
			assertThat(bp.getStatus()).isNotEqualTo(ProjectStatus.DRAFT);
		}
	}

	@Test
	public void shouldReturnBasicProjectByIdWhenIsNotDraft() {
		// given
		BasicProject bp = loadTestReleasedObjectInDatabase();

		// when
		BasicProject basicProject = basicProjectService.findByIdToQuantitiesEvaluation(bp.getId());

		// then
		assertThat(basicProject).extracting("object").contains("Java Rules test!");
		assertThat(basicProject).extracting("requester.id").contains(USER_ID);
		assertThat(basicProject.getStatus()).isNotEqualTo(ProjectStatus.DRAFT);
	}

	@Test
	public void shouldNotReturnBasicProjectByIdWhenIsDraft() {
		// given
		Long id = loadTestBasicProjectInDatabase();

		// when
		BasicProject basicProject = basicProjectService.findByIdToQuantitiesEvaluation(id);

		// then
		assertThat(basicProject).isNull();
	}

	@Test
	public void shouldReturnNullWhenInvalidId() {
		// given
		Long id = 98515L;

		// when
		BasicProject basicProject = basicProjectService.findByIdToQuantitiesEvaluation(id);

		// then
		assertThat(basicProject).isNull();
	}

	@Test
	public void shouldReleaseForFinancialEvaluation() {
		// given
		BasicProject basicProject = loadTestReleasedObjectInDatabase();
		basicProject.getBasicProjectItems().forEach(item -> {
			item = itemService.approveItem(item.getId());
		});
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");

		// when
		basicProject = basicProjectService.releaseForFinancialEvaluation(bindingResult, basicProject.getRequester(),
				basicProject.getId());

		// then
		assertThat(basicProject).isNotNull();
		assertThat(bindingResult.hasGlobalErrors()).isFalse();
		assertThat(basicProject.getStatus()).isEqualTo(ProjectStatus.WAITING_FINANCIAL_HEADING);
	}

	@Test
	public void shouldCancelBascicProjectInRelease() {
		// given
		BasicProject basicProject = loadTestReleasedObjectInDatabase();
		basicProject.getBasicProjectItems().forEach(item -> {
			item = itemService.disapproveItem(item.getId());
		});
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");

		// when
		basicProject = basicProjectService.releaseForFinancialEvaluation(bindingResult, basicProject.getRequester(),
				basicProject.getId());

		// then
		assertThat(basicProject).isNotNull();
		assertThat(bindingResult.hasGlobalErrors()).isFalse();
		assertThat(basicProject.getStatus()).isEqualTo(ProjectStatus.CANCELED);
	}

	@Test
	public void shouldNotReleaseForFinancialEvaluation() {
		// given
		BasicProject basicProject = loadTestReleasedObjectInDatabase();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");

		// when
		basicProject = basicProjectService.releaseForFinancialEvaluation(bindingResult, basicProject.getRequester(),
				basicProject.getId());

		// then
		assertThat(basicProject).isNull();
		assertThat(bindingResult.hasGlobalErrors()).isTrue();
		assertThat(bindingResult.getGlobalError()).extracting("defaultMessage")
				.contains(messages.get("evaluateQuantities.allItemsMustHaveStatus"));
	}

	@Test
	public void shouldNotReleaseForFinancialEvaluationWhenNewBasicProject() {
		// given
		BasicProject basicProject = new BasicProject();
		BindingResult bindingResult = new BeanPropertyBindingResult(basicProject, "basicProject");
		User user = userRepository.findOne(USER_ID);

		// when
		basicProject = basicProjectService.releaseForFinancialEvaluation(bindingResult, user, basicProject.getId());

		// then
		assertThat(basicProject).isNull();
		assertThat(bindingResult.hasGlobalErrors()).isTrue();
		assertThat(bindingResult.getGlobalError()).extracting("defaultMessage")
				.contains(messages.get("record.notFound"));
	}

	@Test
	public void shouldFindBasicProjectsCounters() {
		// given
		User user = userRepository.findOne(USER_ID);

		// when
		List<BasicProject> allProjects = basicProjectRepository
				.findAll(BasicProjectFilter.builder().requesterId(USER_ID).build().toSpec());
		List<Object[]> counters = basicProjectService.findBasicProjectsCounters(user);
		Map<ProjectStatus, Long> groupBy = allProjects.stream()
				.collect(Collectors.groupingBy(BasicProject::getStatus, Collectors.counting()));

		// then
		assertThat(counters).isNotNull();
		for (Object[] obj : counters) {
			assertThat(groupBy.get(obj[0])).isEqualTo(Long.parseLong(obj[1].toString()));
		}
	}

	// TODO nicolas.w req nao funcional - minimo de 50 consultas por segundo
	@Test(timeout = 1000)
	@Repeat(value = 50)
	public void queriesPerSecondTest() {
		assertThat(basicProjectRepository.findAll()).isNotNull();
	}
}
