package br.edu.ifrs.canoas.licitaweb.util;

import static org.assertj.core.api.Assertions.assertThat;

import javax.transaction.Transactional;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class PoiUtilsTest {

	private HSSFWorkbook hssfWorkbook;

	@Test
	public void testGetRow() {
		// given
		hssfWorkbook = new HSSFWorkbook();
		Sheet sheet = hssfWorkbook.createSheet();

		// when
		Row row = PoiUtils.getRow(sheet, 10);
		Row row2 = PoiUtils.getRow(sheet, 10);

		// then
		assertThat(row.getRowNum()).isEqualTo(10);
		assertThat(row2.getRowNum()).isEqualTo(10);
	}

	@Test
	public void testToPrimitiveInt() {
		// given
		Integer x = 1;
		Integer y = null;

		// when
		int i = PoiUtils.toPrimitive(x);
		int j = PoiUtils.toPrimitive(y);

		// then
		assertThat(i).isEqualTo(x);
		assertThat(j).isEqualTo(0);
	}

	@Test
	public void testToPrimitiveDouble() {
		// given
		Double x = 1.0;
		Double y = null;

		// when
		double i = PoiUtils.toPrimitive(x);
		double j = PoiUtils.toPrimitive(y);

		// then
		assertThat(i).isEqualTo(x);
		assertThat(j).isEqualTo(0);
	}
}
