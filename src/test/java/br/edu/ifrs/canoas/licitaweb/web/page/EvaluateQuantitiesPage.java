package br.edu.ifrs.canoas.licitaweb.web.page;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@PageUrl("http://localhost:{port}/evaluate-quantities")
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
public class EvaluateQuantitiesPage extends FluentPage {

	@FindBy(css = ".system-options")
	private FluentWebElement systemOptions;

	@FindBy(css = "#releasedDateStart")
	private FluentWebElement releasedDateStart;

	@FindBy(css = "#releasedDateEnd")
	private FluentWebElement releasedDateEnd;

	@FindBy(css = "#object")
	private FluentWebElement objectInput;

	@FindBy(css = "#submit-search-filters")
	private FluentWebElement submitSearchFilters;

	@FindBy(css = ".table")
	private FluentWebElement table;

	@FindBy(css = "#itemsTableHolder .table tbody tr")
	private FluentList<FluentWebElement> itemsTable;

	@FindBy(css = "#itemsTableHolder .table tbody tr td #btn-approve")
	private List<FluentWebElement> approveBtns;

	@FindBy(css = "#release-form")
	private FluentWebElement releaseToFinancialHeadingBtn;

	@FindBy(css = "#submit-yes")
	private FluentWebElement releaseConfirmBtn;

	@FindBy(css = "#submit-confirmation-modal-content")
	private FluentWebElement releaseConfirmationModal;

	@FindBy(css = "#status")
	private FluentWebElement basicProjectStatus;

	@FindBy(css = ".alert-success span")
	private FluentWebElement releaseSuccessMessage;

	@FindBy(css = ".alert-danger span")
	private FluentWebElement releaseErrorMessage;

	public void isAt() {
		assertThat(window().title()).isEqualTo("Avaliar Quantidades - LicitaWeb");
	}

	// TODO nicolas.w
	public EvaluateQuantitiesPage fillSearchFilters(String object, LocalDate dateStart, LocalDate dateEnd) {
		objectInput.fill().with(object);
		releasedDateStart.getElement().sendKeys(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(dateStart));
		releasedDateEnd.getElement().sendKeys(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(dateEnd));
		return this;
	}

	// TODO nicolas.w
	public EvaluateQuantitiesPage submitSearchFilters() {
		submitSearchFilters.click();
		return this;
	}

	// TODO nicolas.w
	public void goToBasicProjectPage() {
		this.fillSearchFilters("ObJetÓ de Têstes 1", LocalDate.of(2018, 05, 10), LocalDate.of(2018, 05, 10))
				.submitSearchFilters();
		this.getTable().find("#btn-edit").get(0).click();
		await().atMost(5000).until(this.getItemsTable()).present();
	}

	public void goToOtherBasicProjectPage() {
		this.fillSearchFilters("ObJetÓ de Têstes 11", LocalDate.of(2018, 06, 02), LocalDate.of(2018, 06, 01))
		.submitSearchFilters();
		this.getTable().find("#btn-edit").get(0).click();
		await().atMost(5000).until(this.getItemsTable()).present();
	}

	// TODO nicolas.w
	public FluentList<FluentWebElement> getRefreshedItemsTable() {
		return this.asFluentList(getDriver().findElements(By.cssSelector("#itemsTableHolder .table tbody tr")));
	}

	// TODO nicolas.w
	public FluentWebElement getItem(int idx) {
		return this.getItemsTable().get(idx);
	}

	// TODO nicolas.w
	public void clickApproveItemBtn(int idx) {
		this.getItem(idx).el("#btn-approve").click();
	}

	// TODO nicolas.w
	public void clickDispproveItemBtn(int idx) {
		this.getItem(idx).el("#btn-disapprove").click();
	}

	// TODO nicolas.w
	public void clickEditItemBtn(int idx) {
		this.getItem(idx).el("#btn-edit-item").click();
	}

	// TODO nicolas.w
	public void clickConfirmEditItemBtn(int idx) {
		this.getItem(idx).el("#btn-edit-confirm").click();
	}
}