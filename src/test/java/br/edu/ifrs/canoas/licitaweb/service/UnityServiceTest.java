package br.edu.ifrs.canoas.licitaweb.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.ifrs.canoas.licitaweb.domain.Unity;
import br.edu.ifrs.canoas.licitaweb.repository.UnityRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class UnityServiceTest {

	@Autowired
	private UnityService service;
	@Autowired
	private UnityRepository unityRepository;

	private final String NAME_PREFIX = "Unidade ";
	private final Integer NUMBER_OF_ITEMS = 10;

	private List<Unity> loadTestUnits() {
		List<Unity> list = new ArrayList<>();
		for (int i = 1; i <= NUMBER_OF_ITEMS; i++) {
			Unity unity = new Unity();
			unity.setName(NAME_PREFIX + i);
			list.add(unity);
		}
		return list;
	}

	// TODO nicolas
	@Test
	public void shouldReturnListWithAllUnits() {
		// given
		List<Unity> units1 = loadTestUnits();

		// when
		units1 = (List<Unity>) unityRepository.save(units1);
		List<Unity> units2 = service.findAll();

		// then
		assertThat(units2).containsAll(units1);
	}
}
