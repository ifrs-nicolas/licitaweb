package br.edu.ifrs.canoas.licitaweb.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BasicProjectRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private BasicProjectRepository repository;
	@Autowired
	private UnityRepository unityRepository;
	@Autowired
	private UserRepository userRepository;

	private final String OBJECT = "Java Rules!";
	private final String JUSTIFICATION = "JAVA";
	private final BasicProjectType TYPE = BasicProjectType.PRODUCTS;
	private final ProjectStatus STATUS = ProjectStatus.QUANTITY_EVALUATION;
	private final Integer PROCESS_NUMBER = 10;
	private final Integer YEAR = 2001;
	private final Integer UNITY_ID = 100;
	private final Long USER_ID = 1L;

	// TODO RNG007 nicolas
	@Test
	public void whenGetMaxProcessNumberInYear_thenReturnInt() {

		// given
		BasicProject basicProject = new BasicProject();
		basicProject.setObject(OBJECT);
		basicProject.setJustification(JUSTIFICATION);
		basicProject.setSolicitationType(TYPE);
		basicProject.setStatus(STATUS);
		basicProject.setProcessNumberInYear(PROCESS_NUMBER);
		basicProject.setYear(YEAR);
		basicProject.setUnity(unityRepository.findOne(UNITY_ID));
		basicProject.setRequester(userRepository.findOne(USER_ID));
		entityManager.persist(basicProject);
		entityManager.flush();

		// when
		Integer found = repository.getMaxProcessNumberInYear(YEAR);

		// then
		assertThat(found).isNotNull().isEqualTo(10);
	}

	// TODO RNG011 nicolas
	@Test
	public void whenFindByIdAndRequesterId_thenReturnBasicProjectOptional() {

		// given
		BasicProject basicProject = new BasicProject();
		basicProject.setObject(OBJECT);
		basicProject.setJustification(JUSTIFICATION);
		basicProject.setSolicitationType(TYPE);
		basicProject.setStatus(STATUS);
		basicProject.setProcessNumberInYear(PROCESS_NUMBER);
		basicProject.setYear(YEAR);
		basicProject.setUnity(unityRepository.findOne(UNITY_ID));
		basicProject.setRequester(userRepository.findOne(USER_ID));
		entityManager.persist(basicProject);
		entityManager.flush();

		// when
		BasicProject found = repository.findByIdAndRequesterId(basicProject.getId(), USER_ID).get();

		// then
		assertThat(found).isNotNull().extracting("object").contains("Java Rules!");
	}

	@Test
	public void whenFindBasicProjectsCountersByRequesterId_thenReturnListOfCounters() {
		// given
		List<Object[]> foundBefore = repository.findBasicProjectsCounters(USER_ID);
		BasicProject basicProject = new BasicProject();
		basicProject.setObject(OBJECT);
		basicProject.setJustification(JUSTIFICATION);
		basicProject.setSolicitationType(TYPE);
		basicProject.setStatus(STATUS);
		basicProject.setProcessNumberInYear(PROCESS_NUMBER);
		basicProject.setYear(YEAR);
		basicProject.setUnity(unityRepository.findOne(UNITY_ID));
		basicProject.setRequester(userRepository.findOne(USER_ID));

		// when
		entityManager.persist(basicProject);
		entityManager.flush();
		List<Object[]> foundAfter = repository.findBasicProjectsCounters(USER_ID);

		// then
		assertThat(foundBefore).isNotNull();
		assertThat(foundAfter).isNotNull();

		boolean fail = true;
		for (Object[] objAfter : foundAfter) {
			if (objAfter[0].equals(STATUS)) {
				for (Object[] objBefore : foundBefore) {
					if (objBefore[0].equals(STATUS)) {
						assertThat(Integer.parseInt(objAfter[1].toString())).isEqualTo(Integer.parseInt(objBefore[1].toString()) + 1);
						fail = false;
					}
				}
			}
		}
		assertThat(fail).isFalse();
	}
}