package br.edu.ifrs.canoas.licitaweb.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.ifrs.canoas.licitaweb.domain.SighProduct;
import br.edu.ifrs.canoas.licitaweb.repository.SighProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class SighProductServiceTest {

	@Autowired
	private SighProductService service;
	@Autowired
	private SighProductRepository repository;

	private final String NAME_PREFIX = "Produto junit ";
	private final Integer NUMBER_OF_ITEMS = 10;

	private List<SighProduct> loadTestProducts() {
		List<SighProduct> list = new ArrayList<>();
		for (int i = 1; i <= NUMBER_OF_ITEMS; i++) {
			SighProduct product = new SighProduct();
			product.setName(NAME_PREFIX + i);
			product.setAveragePrice(Double.valueOf(i));
			product.setDescription(product.getName());
			list.add(product);
		}
		return list;
	}

	// TODO nicolas
	@Test
	public void shouldReturnListWithAllProducts() {
		// given
		List<SighProduct> products1 = loadTestProducts();

		// when
		products1 = (List<SighProduct>) repository.save(products1);
		List<SighProduct> products2 = service.findAll();

		// then
		assertThat(products2).containsAll(products1);
	}
}
