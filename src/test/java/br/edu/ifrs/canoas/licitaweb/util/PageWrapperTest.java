package br.edu.ifrs.canoas.licitaweb.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.ifrs.canoas.licitaweb.domain.User;
import br.edu.ifrs.canoas.licitaweb.dto.PageWrapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class PageWrapperTest {

	private List<User> loadTestArray() {
		List<User> content = new ArrayList<>();
		for (int i = 1; i <= 100; i++) {
			User u = new User();
			u.setName("test obj " + i);
			content.add(u);
		}
		return content;
	}

	// TODO nicolas
	@Test
	public void shouldReturnPageWrapperWithAllContent() {
		// given
		List<User> content = loadTestArray();
		Sort sort = new Sort("name");
		Pageable pageable = PageRequest.of(0, 100, sort);
		Page<User> page = new PageImpl<>(content, pageable, content.size());

		// when
		PageWrapper<User> wrapper = new PageWrapper<>("users", page, null);

		// then
		assertThat(wrapper.getContent()).containsAll(content);
		assertThat(wrapper.getSortDir(null)).isNull();
		assertThat(wrapper.getSortDir("invalid property name")).isNull();
		assertThat(wrapper.getSortDir("name")).isEqualTo(Sort.Direction.ASC.name());
	}

	// TODO nicolas
	@Test
	public void shouldReturnNullDirWhenNoSort() {
		// given
		List<User> content = loadTestArray();
		Sort sort = null;
		Pageable pageable = PageRequest.of(0, 2, sort);
		Page<User> page = new PageImpl<>(content, pageable, content.size());

		// when
		PageWrapper<User> wrapper = new PageWrapper<>("users", page, null);

		// then
		assertThat(wrapper.getSortDir("name")).isNull();
	}

	// TODO nicolas
	@Test
	public void shouldOverrideDefaultPageOptions() {
		// given
		List<User> content = loadTestArray();
		Sort sort = null;
		Pageable pageable = PageRequest.of(0, 10, sort);
		Page<User> page = new PageImpl<>(content, pageable, content.size());
		int[] custonPageOptions = { 10, 15 };

		// when
		PageWrapper<User> wrapper = new PageWrapper<>("users", page, custonPageOptions);

		// then
		assertThat(wrapper.getPageOptions()).containsExactlyInAnyOrder(custonPageOptions);
	}

	// TODO nicolas
	@Test
	public void shouldRenurnNullContentWhenPageIsNull() {
		// given
		Page<User> page = null;

		// when
		PageWrapper<User> wrapper = new PageWrapper<>("users", page, null);

		// then
		assertThat(wrapper.getContent()).isNull();
	}

	// TODO nicolas
	@Test
	public void shouldVerifyPaginationbarStart_1() {
		// given
		List<User> content = loadTestArray();
		Sort sort = null;
		Pageable pageable = PageRequest.of(2, 5, sort);
		Page<User> page = new PageImpl<>(content, pageable, content.size());
		int[] custonPageOptions = { 10, 15 };

		// when
		PageWrapper<User> wrapper = new PageWrapper<>("users", page, custonPageOptions);

		// then
		assertThat(wrapper.getPaginationbarStart()).isEqualTo(1);
	}

	// TODO nicolas
	@Test
	public void shouldVerifyPaginationbarStart_2() {
		// given
		List<User> content = loadTestArray();
		Sort sort = null;
		Pageable pageable = PageRequest.of(19, 5, sort);
		Page<User> page = new PageImpl<>(content, pageable, content.size());
		int[] custonPageOptions = { 10, 15 };

		// when
		PageWrapper<User> wrapper = new PageWrapper<>("users", page, custonPageOptions);

		// then
		assertThat(wrapper.getPaginationbarStart()).isEqualTo(16);
	}

	// TODO nicolas
	@Test
	public void shouldVerifyPaginationbarStart_3() {
		// given
		List<User> content = loadTestArray();
		Sort sort = null;
		Pageable pageable = PageRequest.of(11, 5, sort);
		Page<User> page = new PageImpl<>(content, pageable, content.size());
		int[] custonPageOptions = { 10, 15 };

		// when
		PageWrapper<User> wrapper = new PageWrapper<>("users", page, custonPageOptions);

		// then
		assertThat(wrapper.getPaginationbarStart()).isEqualTo(10);
	}

	// TODO nicolas
	@Test
	public void shouldVerifyPaginationbarBarsizeWhenEmptyContent() {
		// given
		List<User> content = new ArrayList<>();
		Page<User> page = new PageImpl<>(content, PageRequest.of(0, 10, null), content.size());

		// when
		PageWrapper<User> wrapper = new PageWrapper<>("users", page, null);

		// then
		assertThat(wrapper.getPaginationbarSize()).isEqualTo(1);
	}
}
