package br.edu.ifrs.canoas.licitaweb.web.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import br.edu.ifrs.canoas.licitaweb.web.config.MyFluentTest;
import br.edu.ifrs.canoas.licitaweb.web.page.BasicProjectPage;
import br.edu.ifrs.canoas.licitaweb.web.page.NewBasicProjectPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BasicProjectTest extends MyFluentTest {

	@Page
	private BasicProjectPage basicProjectPage;

	@Page
	private NewBasicProjectPage newBasicProjectPage;

	// TODO nicolas.w CdT001 - RNG001
	@Test
	public void test1_submitNewBasicProject() {
		// Given
		loginUser();
		newBasicProjectPage.go(port);
		newBasicProjectPage.isAt();
		newBasicProjectPage.fillForm(1, "obj teste selenium", "justificativa teste selenium");

		// When
		newBasicProjectPage.submitForm();

		// Then
		assertThat(newBasicProjectPage.getSuccessMessage().present()).isTrue();
		assertThat(newBasicProjectPage.getSuccessMessage().getElement().getText())
				.isEqualTo("Projeto básico salvo com sucesso!");
	}

	// TODO nicolas.w CdT001 - RNG005, RNG006
	@Test
	public void test2_releaseForEvaluationWhenEmptyItems() {
		// Given
		loginUser();
		newBasicProjectPage.go(port);
		newBasicProjectPage.isAt();
		newBasicProjectPage.fillForm(1, "obj teste selenium", "justificativa teste selenium").submitForm();

		// When
		newBasicProjectPage.releaseForEavluation();
		await().atMost(500, TimeUnit.MILLISECONDS).until(newBasicProjectPage.getReleaseModalConfirmation()).displayed();
		assertThat(newBasicProjectPage.getReleaseModalConfirmation().displayed()).isTrue(); // RNG005
		newBasicProjectPage.releaseConfirmYes();

		// Then
		await().atMost(500, TimeUnit.MILLISECONDS).until(newBasicProjectPage.getErrorMessage()).displayed();
		assertThat(newBasicProjectPage.getErrorMessage().displayed()).isTrue();
		assertThat(newBasicProjectPage.getErrorMessage().getElement().getText())
				.isEqualTo("Projeto básico deve possuir ao menos um item!"); // RNG006
	}

	// TODO nicolas.w CdT001 - RNG004
	@Test
	public void test3_addItemInNewBasicProject() {
		// Given
		loginUser();
		newBasicProjectPage.go(port);
		newBasicProjectPage.isAt();
		newBasicProjectPage.fillForm(1, "obj teste selenium", "justificativa teste selenium").submitForm();
		newBasicProjectPage.getAddItem().click();
		newBasicProjectPage.fillModal(1, 10.0);

		// When
		newBasicProjectPage.submitModal();

		// Then
		assertThat(newBasicProjectPage.getSuccessMessage().present()).isTrue();
		assertThat(newBasicProjectPage.getSuccessMessage().getElement().getText())
				.isEqualTo("Item adicionado com sucesso ao projeto básico.");
		assertThat(new Select(newBasicProjectPage.getSighProduct().getElement()).getFirstSelectedOption().getText())
				.isEqualTo("Selecione um produto");
		assertThat(newBasicProjectPage.getQuantity().getElement().getText()).isEmpty();
	}

	// TODO nicolas.w CdT001 - RNG010
	@Test
	public void test4_addSameItemTwiceInBasicProject() {
		// Given
		loginUser();
		newBasicProjectPage.go(port);
		newBasicProjectPage.isAt();
		newBasicProjectPage.fillForm(1, "obj teste selenium", "justificativa teste selenium").submitForm();
		newBasicProjectPage.getAddItem().click();
		newBasicProjectPage.fillModal(1, 10.0).submitModal();
		newBasicProjectPage.fillModal(1, 8.0);
		// When
		newBasicProjectPage.submitModal();

		// Then
		assertThat(newBasicProjectPage.getErrorMessage().present()).isTrue();
		assertThat(newBasicProjectPage.getErrorMessage().getElement().getText())
				.isEqualTo("Não é permitido repetir o mesmo código do item em um mesmo projeto básico.");
	}

	// TODO nicolas.w CdT001 - RNG012
	@Test
	public void test5_checkSearchFilters() {
		// Given
		loginUser();
		basicProjectPage.go(port);
		basicProjectPage.isAt();
		basicProjectPage.fillSearchFilters("ObJetÓ de Têstes 14", "justIficândo o teste 14");

		// When
		basicProjectPage.submitSearchFilters();

		// Then
		List<FluentWebElement> trs = basicProjectPage.getTable().find(By.xpath("//tbody//tr"));
		assertThat(trs.size()).isEqualTo(1);
		assertThat(trs.get(0).find(By.xpath("//td")).get(2).getElement().getText()).isEqualTo("Objeto de Testes 14");
	}
}