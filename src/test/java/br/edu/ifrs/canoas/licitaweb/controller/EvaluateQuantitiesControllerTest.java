package br.edu.ifrs.canoas.licitaweb.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;
import br.edu.ifrs.canoas.licitaweb.domain.Unity;
import br.edu.ifrs.canoas.licitaweb.report.BasicProjectItemsExcelView;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectItemService;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectService;
import br.edu.ifrs.canoas.licitaweb.service.UnityService;

@WebMvcTest(EvaluateQuantitiesController.class)
@EnableSpringDataWebSupport
public class EvaluateQuantitiesControllerTest extends BaseControllerTest {

	@MockBean
	private Messages messages;
	@MockBean
	private BasicProjectService basicProjectService;
	@MockBean
	private BasicProjectItemService basicProjectItemService;
	@MockBean
	private UnityService unityService;
	@MockBean
	private BasicProjectItemsExcelView excelView;

	private BasicProject testObject() {
		BasicProject basicProject = new BasicProject();
		basicProject.setId(1L);
		basicProject.setObject("Test controller object");
		basicProject.setJustification("Teste justify");
		Unity unity = new Unity();
		unity.setId(1);
		unity.setName("HMGV");
		basicProject.setUnity(unity);
		basicProject.setSolicitationType(BasicProjectType.PRODUCTS);
		return basicProject;
	}

	private BasicProjectItem testItem() {
		BasicProjectItem item = new BasicProjectItem();
		item.setId(100L);
		item.setItemNumber(100);
		item.setName("item name");
		item.setDescription("item description");
		item.setQuantity(10.0);
		return item;
	}

	@Test
	public void view_list_page() throws Exception {
		when(basicProjectService.findToQuantitiesEvaluation(any(), any()))
				.thenReturn(new PageImpl<>(new ArrayList<>()));
		this.mvc.perform(get("/evaluate-quantities").with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(status().isOk()).andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/evaluate-quantities/list"));
	}

	@Test
	public void view_form_edit_page() throws Exception {
		Long id = 1L;
		when(unityService.findAll()).thenReturn(new ArrayList<>());
		when(basicProjectService.findByIdToQuantitiesEvaluation(id)).thenReturn(mock(BasicProject.class));

		// WHEN FOUND THE OBJECT
		this.mvc.perform(get("/evaluate-quantities/" + id).with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(status().isOk()).andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/evaluate-quantities/form"));

		// WHEN NOT FOUND THE OBJECT
		when(basicProjectService.findByIdToQuantitiesEvaluation(id)).thenReturn(null);
		this.mvc.perform(get("/evaluate-quantities/" + id).with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(view().name("redirect:/evaluate-quantities"));
	}

	@Test
	public void release_for_finacial_evaluation() throws Exception {
		BasicProject basicProject = testObject();
		basicProject.setStatus(ProjectStatus.WAITING_FINANCIAL_HEADING);
		when(messages.get("evaluateQuantities.releasedToFinancialEvaluation"))
				.thenReturn("evaluateQuantities.releasedToFinancialEvaluation");
		when(basicProjectService.releaseForFinancialEvaluation(any(), any(), any())).thenReturn(basicProject);

		this.mvc.perform(post("/evaluate-quantities/release").with(user(userDetails)).with(csrf())
				.param("id", basicProject.getId().toString()).param("object", basicProject.getObject())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
				.andExpect(view().name("redirect:/evaluate-quantities/" + basicProject.getId()))
				.andExpect(flash().attribute("message", "evaluateQuantities.releasedToFinancialEvaluation"));
	}

	@Test
	public void cancel_project_in_elease_to_finacial_evaluation() throws Exception {
		BasicProject basicProject = testObject();
		basicProject.setStatus(ProjectStatus.CANCELED);
		when(messages.get("evaluateQuantities.canceled"))
		.thenReturn("evaluateQuantities.canceled");
		when(basicProjectService.releaseForFinancialEvaluation(any(), any(), any())).thenReturn(basicProject);

		this.mvc.perform(post("/evaluate-quantities/release").with(user(userDetails)).with(csrf())
				.param("id", basicProject.getId().toString()).param("object", basicProject.getObject())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("user", userDetails.getUser()))
		.andExpect(view().name("redirect:/evaluate-quantities/" + basicProject.getId()))
		.andExpect(flash().attribute("message", "evaluateQuantities.canceled"));
	}

	@Test
	public void release_for_finacial_evaluation_errors() throws Exception {
		BasicProject basicProject = testObject();
		when(basicProjectService.releaseForEvaluation(any(), any(), any())).thenReturn(null);
		when(basicProjectService.findById(any())).thenReturn(Optional.of(basicProject));

		this.mvc.perform(post("/evaluate-quantities/release").with(user(userDetails)).with(csrf())
				.param("id", basicProject.getId().toString()).contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.sessionAttr("user", userDetails.getUser())).andExpect(view().name("/evaluate-quantities/form"));
	}

	@Test
	public void edit_quantity() throws Exception {
		BasicProjectItem item = testItem();
		item.setQuantityApproved(10.0);
		item.setBasicProject(testObject());
		when(basicProjectItemService.changeQuantity(any())).thenReturn(item);
		this.mvc.perform(post("/evaluate-quantities/edit-quantity").with(user(userDetails)).with(csrf())
				.accept(MediaType.TEXT_HTML).param("id", item.getId().toString())
				.param("quantityApproved", item.getQuantityApproved().toString())).andExpect(status().isOk())
				.andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/evaluate-quantities/fragments/itemRow :: itemRow"));
	}

	@Test
	public void approve_item() throws Exception {
		BasicProjectItem item = testItem();
		item.setBasicProject(testObject());
		when(basicProjectItemService.approveItem(any())).thenReturn(item);
		this.mvc.perform(post("/evaluate-quantities/approve").with(user(userDetails)).with(csrf())
				.accept(MediaType.TEXT_HTML).param("id", item.getId().toString())).andExpect(status().isOk())
				.andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/evaluate-quantities/fragments/itemRow :: itemRow"));
	}

	@Test
	public void disapprove_item() throws Exception {
		BasicProjectItem item = testItem();
		item.setBasicProject(testObject());
		when(basicProjectItemService.disapproveItem(any())).thenReturn(item);
		this.mvc.perform(post("/evaluate-quantities/disapprove").with(user(userDetails)).with(csrf())
				.accept(MediaType.TEXT_HTML).param("id", item.getId().toString())).andExpect(status().isOk())
				.andExpect(content().contentType("text/html;charset=UTF-8"))
				.andExpect(view().name("/evaluate-quantities/fragments/itemRow :: itemRow"));
	}

	@Test
	public void download_xls() throws Exception {
		BasicProject basicProject =  testObject();
		when(basicProjectService.findByIdToQuantitiesEvaluation(basicProject.getId()))
				.thenReturn(basicProject);

		// WHEN FOUND THE OBJECT
		this.mvc.perform(get("/evaluate-quantities/download").with(user(userDetails)).accept(MediaType.TEXT_HTML)
				.param("id", basicProject.getId().toString()))
				.andExpect(status().isOk());

		// WHEN NOT FOUND THE OBJECT
		when(basicProjectService.findByIdToQuantitiesEvaluation(basicProject.getId())).thenReturn(null);
		this.mvc.perform(get("/evaluate-quantities/download").with(user(userDetails)).accept(MediaType.TEXT_HTML))
				.andExpect(view().name("redirect:/evaluate-quantities"));
	}
}
