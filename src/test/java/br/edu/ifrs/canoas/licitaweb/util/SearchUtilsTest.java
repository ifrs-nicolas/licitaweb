package br.edu.ifrs.canoas.licitaweb.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class SearchUtilsTest {

	@Mock(extraInterfaces = Serializable.class)
	Root<Object> root;
	@Mock(extraInterfaces = Serializable.class)
	CriteriaQuery<?> query;
	@Mock(extraInterfaces = Serializable.class)
	CriteriaBuilder builder;

	// TODO nicolas
	@Test
	public void testPredicatesSize() {
		// given
		String object = "testObject";
		Long id = 1L;
		Integer year = 2018;
		String strNull = null;
		Integer intNull = null;
		Long longNull = null;
		Boolean boolNull = null;

		// when
		Collection<Predicate> predicates = new ArrayList<>();
		SearchUtils.search(predicates, builder, root, "object", object);
		SearchUtils.search(predicates, builder, root, "id", id);
		SearchUtils.search(predicates, builder, root, "year", year);
		SearchUtils.search(predicates, builder, root, "deleted", Boolean.FALSE);
		SearchUtils.search(predicates, builder, root, "inactive", Boolean.TRUE);
		SearchUtils.search(predicates, builder, root, "testStr", strNull);
		SearchUtils.search(predicates, builder, root, "testStr2", " ");
		SearchUtils.search(predicates, builder, root, "testInt", intNull);
		SearchUtils.search(predicates, builder, root, "testLong", longNull);
		SearchUtils.search(predicates, builder, root, "testBool", boolNull);

		// then
		assertThat(predicates).hasSize(5);
	}

	@Test
	public void testDateSearch() {
		// given
		LocalDate start = LocalDate.now().minusYears(1);
		LocalDate end = LocalDate.now();
		LocalDate dateNull = null;

		// when
		Collection<Predicate> predicates = new ArrayList<>();
		SearchUtils.search(predicates, builder, root, "createdDate", start, end);
		SearchUtils.search(predicates, builder, root, "createdDate", start, dateNull);
		SearchUtils.search(predicates, builder, root, "createdDate", dateNull, end);
		SearchUtils.search(predicates, builder, root, "createdDate", end, start);
		SearchUtils.search(predicates, builder, root, "createdDate", dateNull, dateNull);

		// then
		assertThat(predicates).hasSize(4);
	}
}
