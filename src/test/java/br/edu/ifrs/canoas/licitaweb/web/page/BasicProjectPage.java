package br.edu.ifrs.canoas.licitaweb.web.page;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@PageUrl("http://localhost:{port}/basic-project")
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
public class BasicProjectPage extends FluentPage {

	@FindBy(css = "#object")
	private FluentWebElement objectInput;

	@FindBy(css = "#justification")
	private FluentWebElement justificationInput;

	@FindBy(css = "#submit-search-filters")
	private FluentWebElement submitSearchFilters;

	@FindBy(css = ".table")
	private FluentWebElement table;

	public void isAt() {
		assertThat(window().title()).isEqualTo("Meus Projetos Básicos - LicitaWeb");
	}

	//TODO CdT001 nicolas.w
	public BasicProjectPage fillSearchFilters(String object, String justification) {
		objectInput.fill().with(object);
		justificationInput.fill().with(justification);
		return this;
	}

	//TODO CdT001 nicolas.w
	public BasicProjectPage submitSearchFilters() {
		submitSearchFilters.click();
		return this;
	}
}