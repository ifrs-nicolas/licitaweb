package br.edu.ifrs.canoas.licitaweb.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BasicProjectItemRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private BasicProjectItemRepository repository;
	@Autowired
	private UnityRepository unityRepository;
	@Autowired
	private UserRepository userRepository;

	private final String OBJECT = "Java Rules!";
	private final String JUSTIFICATION = "JAVA";
	private final BasicProjectType TYPE = BasicProjectType.PRODUCTS;
	private final ProjectStatus STATUS = ProjectStatus.QUANTITY_EVALUATION;
	private final Integer PROCESS_NUMBER = 10;
	private final Integer YEAR = 2001;
	private final Integer UNITY_ID = 100;
	private final Long USER_ID = 1L;

	private final String ITEM_NAME = "test item name";
	private final Integer ITEM_NUMBER = 5568;

	// TODO RNG010 nicolas
	@Test
	public void when_FindByBasicProjectIdAndItemNumber_then_ReturnOptional() {
		// given
		BasicProject basicProject = new BasicProject();
		basicProject.setObject(OBJECT);
		basicProject.setJustification(JUSTIFICATION);
		basicProject.setSolicitationType(TYPE);
		basicProject.setStatus(STATUS);
		basicProject.setProcessNumberInYear(PROCESS_NUMBER);
		basicProject.setYear(YEAR);
		basicProject.setUnity(unityRepository.findOne(UNITY_ID));
		basicProject.setRequester(userRepository.findOne(USER_ID));
		entityManager.persist(basicProject);
		entityManager.flush();

		BasicProjectItem item = new BasicProjectItem();
		item.setBasicProject(basicProject);
		item.setName(ITEM_NAME);
		item.setItemNumber(ITEM_NUMBER);
		item.setDescription(ITEM_NAME);
		item.setQuantity(1.0);
		entityManager.persist(item);
		entityManager.flush();

		// when
		Optional<BasicProjectItem> found = repository.findByBasicProjectIdAndItemNumber(basicProject.getId(),
				ITEM_NUMBER);

		// then
		assertThat(found.get().getName()).isEqualTo(ITEM_NAME);
	}

	// TODO RNG010 nicolas
	@Test
	public void given_noData_when_FindByBasicProjectIdAndItemNumber_then_ReturnEmptyOptional() {
		// given

		// when
		Optional<BasicProjectItem> found = repository.findByBasicProjectIdAndItemNumber(1054158L, ITEM_NUMBER);

		// then
		assertThat(found).isEmpty();
	}

}