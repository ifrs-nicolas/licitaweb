package br.edu.ifrs.canoas.licitaweb.web.page;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@PageUrl("http://localhost:{port}/basic-project/new")
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
public class NewBasicProjectPage extends FluentPage {

	@FindBy(css = "#solicitationType1")
	private FluentWebElement solicitationType1;

	@FindBy(css = "#unity")
	private FluentWebElement unity;

	@FindBy(css = "#object")
	private FluentWebElement object;

	@FindBy(css = "#justification")
	private FluentWebElement justification;

	@FindBy(css = "#submit-form")
	private FluentWebElement submitForm;

	@FindBy(css = "#release-form")
	private FluentWebElement releaseForm;

	@FindBy(css = "#submit-confirmation-modal-content")
	private FluentWebElement releaseModalConfirmation;

	@FindBy(css = "#submit-yes")
	private FluentWebElement releaseConfirmationBtn;

	@FindBy(css = ".table")
	private FluentWebElement table;

	@FindBy(css = ".alert-success>span")
	private FluentWebElement successMessage;

	@FindBy(css = ".alert-danger>span")
	private FluentWebElement errorMessage;

	@FindBy(css = "#addItem")
	private FluentWebElement addItem;

	@FindBy(css = "#sighProduct")
	private FluentWebElement sighProduct;

	@FindBy(css = "#quantity")
	private FluentWebElement quantity;

	@FindBy(css = "#submit-modal")
	private FluentWebElement submitModal;

	public void isAt() {
		assertThat(window().title()).isEqualTo("Novo Projeto Básico - LicitaWeb");
	}

	//TODO CdT001 nicolas.w
	public NewBasicProjectPage fillForm(int idx, String object, String justification) {
		this.solicitationType1.click();
		this.unity.fillSelect().withIndex(idx);
		this.object.fill().with(object);
		this.justification.fill().with(justification);
		return this;
	}

	//TODO CdT001 nicolas.w
	public NewBasicProjectPage submitForm() {
		this.submitForm.click();
		return this;
	}

	//TODO CdT001 nicolas.w
	public NewBasicProjectPage releaseForEavluation() {
		this.releaseForm.click();
		return this;
	}

	//TODO CdT001 nicolas.w
	public NewBasicProjectPage releaseConfirmYes() {
		this.releaseConfirmationBtn.click();
		return this;
	}

	//TODO CdT001 nicolas.w
	public NewBasicProjectPage fillModal(int idx, Double quantity) {
		this.sighProduct.fillSelect().withIndex(idx);
		this.quantity.fill().with(quantity.toString());
		return this;
	}

	//TODO CdT001 nicolas.w
	public NewBasicProjectPage submitModal() {
		this.submitModal.click();
		return this;
	}
}