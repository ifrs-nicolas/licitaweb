package br.edu.ifrs.canoas.licitaweb.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

public class SearchUtils {

	private static final String TRANSLATE_FROM = "'áàâãäéèêëíìïóòôõöúùûüÁÀÂÃÄÉÈÊËÍÌÏÓÒÔÕÖÚÙÛÜçÇ'";
	private static final String TRANSLATE_TO = "'aaaaaeeeeiiiooooouuuuAAAAAEEEEIIIOOOOOUUUUcC'";

	private SearchUtils() {
	}

	// TODO RNG012 nicolas
	public static Expression<String> removeAccents(CriteriaBuilder builder, Expression<String> expression) {
		return builder.function("translate", String.class, expression, builder.literal(TRANSLATE_FROM),
				builder.literal(TRANSLATE_TO));
	}

	// TODO RNG012 nicolas
	public static void search(Collection<Predicate> predicates, CriteriaBuilder builder, Path<?> path, String collunm,
			String parameter) {
		if (parameter == null || parameter.trim().isEmpty())
			return;
		predicates.add(builder.like(removeAccents(builder, builder.upper(path.<String>get(collunm))),
				removeAccents(builder, builder.upper(builder.literal("%" + parameter + "%")))));
	}

	public static void search(Collection<Predicate> predicates, CriteriaBuilder builder, Path<?> path, String collunm,
			Integer parameter) {
		if (parameter == null)
			return;
		predicates.add(builder.equal(path.get(collunm), parameter.intValue()));
	}

	public static void search(Collection<Predicate> predicates, CriteriaBuilder builder, Path<?> path, String collunm,
			Long parameter) {
		if (parameter == null)
			return;
		predicates.add(builder.equal(path.get(collunm), parameter.longValue()));
	}

	public static void search(Collection<Predicate> predicates, CriteriaBuilder builder, Path<?> path, String collunm,
			LocalDate startParam, LocalDate endParam) {
		LocalDateTime startDate = startParam == null ? null : LocalDateTime.of(startParam, LocalTime.MIN);
		LocalDateTime endDate = endParam == null ? null : LocalDateTime.of(endParam, LocalTime.MAX);
		if (startDate == null && endDate != null)
			predicates.add(builder.lessThanOrEqualTo(path.get(collunm), endDate));
		else if (startDate != null && endDate == null)
			predicates.add(builder.greaterThanOrEqualTo(path.get(collunm), startDate));
		else if (startDate != null && endDate != null) {
			if (startDate.isAfter(endDate)) {
				predicates.add(builder.greaterThanOrEqualTo(path.get(collunm), startDate));
			} else {
				predicates.add(builder.between(path.get(collunm), startDate, endDate));
			}
		}
	}

	public static void search(Collection<Predicate> predicates, CriteriaBuilder builder, Path<?> path, String collunm,
			Boolean parameter) {
		if (parameter == null) {
			return;
		} else if (parameter) {
			predicates.add(builder.isTrue(path.get(collunm)));
		} else {
			predicates.add(builder.isFalse(path.get(collunm)));
		}
	}
}
