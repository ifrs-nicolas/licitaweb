package br.edu.ifrs.canoas.licitaweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.config.auth.UserImpl;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.dto.PageWrapper;
import br.edu.ifrs.canoas.licitaweb.filter.BasicProjectFilter;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectItemService;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectService;
import br.edu.ifrs.canoas.licitaweb.service.UnityService;
import lombok.AllArgsConstructor;

@RequestMapping("/financial-headings")
@Controller
@AllArgsConstructor
public class EvaluateFinancialHeadingsController {

	private final Messages messages;
	private final BasicProjectService basicProjectService;
	private final BasicProjectItemService basicProjectItemService;
	private final UnityService unityService;

	// TODO nicolas
	@GetMapping(value = { "", "/", "/approve-for-biddings", "/return-to-quantities-evaluation" })
	public ModelAndView listPage(@AuthenticationPrincipal UserImpl activeUser,
			@PageableDefault(page = 0, value = 5, sort = "releasedDateToFinancialEvaluation", direction = Direction.ASC) Pageable pageable,
			BasicProjectFilter filter) {
		ModelAndView mav = new ModelAndView("/evaluate-financial-headings/list");
		mav.addObject("page", new PageWrapper<>("financial-headings",
				basicProjectService.findToFinancialHeadingsEvaluation(filter, pageable)));
		mav.addObject("filter", filter);
		return mav;
	}

	// TODO nicolas
	private ModelAndView formPage() {
		ModelAndView mav = new ModelAndView("/evaluate-financial-headings/form");
		mav.addObject("basicProject", new BasicProject());
		mav.addObject("basicProjectItem", new BasicProjectItem());
		mav.addObject("units", unityService.findAll());
		return mav;
	}

	// TODO nicolas
	@GetMapping("/{id}")
	public ModelAndView formEditPage(@AuthenticationPrincipal UserImpl activeUser, @PathVariable Long id) {
		BasicProject basicProject = basicProjectService.findByIdToFinancialHeadingsEvaluation(id);
		if (basicProject != null) {
			ModelAndView mav = formPage();
			mav.addObject("basicProject", basicProject);
			return mav;
		}
		return new ModelAndView("redirect:/financial-headings");
	}

	@PostMapping(path = "/approve-for-biddings")
	public ModelAndView approveForBiddings(@AuthenticationPrincipal UserImpl activeUser, BasicProject basicProject,
			RedirectAttributes redirectAttr) {
		List<String> errors = new ArrayList<>();
		basicProject = basicProjectService.approveForBidding(errors, activeUser.getUser(), basicProject.getId());
		if (!errors.isEmpty())
			return this.formPage()
					.addObject("basicProject", basicProject)
					.addObject("errors", errors);
		ModelAndView mav = new ModelAndView("redirect:/financial-headings/" + basicProject.getId());
		String msg = messages.get("financialHeadings.approvedForBiddings");
		redirectAttr.addFlashAttribute("message", msg);
		return mav;
	}

	@PostMapping("/return-to-quantities-evaluation")
	public ModelAndView returnToQuantitiesEvaluation(@AuthenticationPrincipal UserImpl activeUser,
			BasicProject basicProject, RedirectAttributes redirectAttr) {
		BasicProject updatedBP = basicProjectService.returnToQuantitiesEvaluation(activeUser.getUser(), basicProject);
		if (updatedBP == null)
			return this.formPage().addObject("basicProject", new BasicProject());
		ModelAndView mav = new ModelAndView("redirect:/financial-headings/" + updatedBP.getId());
		redirectAttr.addFlashAttribute("message", messages.get("financialHeadings.approvedForBiddings"));
		return mav;
	}

	@PostMapping("/change-financial-heading")
	public ModelAndView editItemQuantity(@AuthenticationPrincipal UserImpl activeUser,
			BasicProjectItem basicProjectItem) {
		ModelAndView mav = new ModelAndView("/evaluate-financial-headings/fragments/itemRow :: itemRow");
		mav.addObject("item", basicProjectItemService.changeFinancialHeading(basicProjectItem));
		return mav;
	}
}
