package br.edu.ifrs.canoas.licitaweb.domain;

public enum ProjectStatus {
	DRAFT, QUANTITY_EVALUATION, WAITING_FINANCIAL_HEADING, APPROVED_FOR_BIDDING, CANCELED, QUANTITIES_REVALUATION;
}
