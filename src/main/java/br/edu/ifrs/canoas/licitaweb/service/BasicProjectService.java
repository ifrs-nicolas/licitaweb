package br.edu.ifrs.canoas.licitaweb.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;
import br.edu.ifrs.canoas.licitaweb.domain.User;
import br.edu.ifrs.canoas.licitaweb.filter.BasicProjectFilter;
import br.edu.ifrs.canoas.licitaweb.repository.BasicProjectItemRepository;
import br.edu.ifrs.canoas.licitaweb.repository.BasicProjectRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class BasicProjectService {

	private final BasicProjectRepository basicProjectRepository;
	private final BasicProjectItemRepository basicProjectItemRepository;
	private final Messages messages;

	// TODO RNG012 nicolas
	public Page<BasicProject> findAll(BasicProjectFilter filter, Pageable pageable) {
		Specification<BasicProject> specification = filter.toSpec();
		Page<BasicProject> page = basicProjectRepository.findAll(specification, pageable);
		while (!page.isFirst() && page.getContent().isEmpty()) {
			page = basicProjectRepository.findAll(specification, page.previousPageable());// replace to getTotalPage()
		}
		return page;
	}

	public Page<BasicProject> findAllByUser(BasicProjectFilter filter, Pageable pageable, User user) {
		filter.setRequesterId(user.getId());
		return this.findAll(filter, pageable);
	}

	public Page<BasicProject> findToQuantitiesEvaluation(BasicProjectFilter filter, Pageable pageable) {
		if (filter.getNotEqualStatus() == null)
			filter.setNotEqualStatus(new ArrayList<>());
		filter.getNotEqualStatus().add(ProjectStatus.DRAFT.toString());
		return this.findAll(filter, pageable);
	}

	public Page<BasicProject> findToFinancialHeadingsEvaluation(BasicProjectFilter filter, Pageable pageable) {
		if (filter.getNotEqualStatus() == null)
			filter.setNotEqualStatus(new ArrayList<>());
		filter.getNotEqualStatus().add(ProjectStatus.DRAFT.toString());
		filter.getNotEqualStatus().add(ProjectStatus.QUANTITY_EVALUATION.toString());
		filter.getNotEqualStatus().add(ProjectStatus.QUANTITIES_REVALUATION.toString());
		filter.getNotEqualStatus().add(ProjectStatus.CANCELED.toString());
		return this.findAll(filter, pageable);
	}

	// TODO nicolas
	public Optional<BasicProject> findById(Long id) {
		return id == null ? Optional.empty() : Optional.ofNullable(basicProjectRepository.findOne(id));
	}

	// TODO RNG0011 nicolas
	public Optional<BasicProject> findByIdAndRequester(Long id, User user) {
		if (user == null) {
			return Optional.empty();
		}
		return basicProjectRepository.findByIdAndRequesterId(id, user.getId());
	}

	public BasicProject findByIdToQuantitiesEvaluation(Long id) {
		Optional<BasicProject> basicProject = this.findById(id);
		if (!basicProject.isPresent() || basicProject.get().getStatus().equals(ProjectStatus.DRAFT))
			return null;
		return basicProject.get();
	}

	public BasicProject findByIdToFinancialHeadingsEvaluation(Long id) {
		Optional<BasicProject> basicProject = this.findById(id);
		if (!basicProject.isPresent()
				|| (!ProjectStatus.WAITING_FINANCIAL_HEADING.equals(basicProject.get().getStatus())
						&& !ProjectStatus.APPROVED_FOR_BIDDING.equals(basicProject.get().getStatus())))
			return null;
		return basicProject.get();
	}

	// TODO nicolas
	public boolean deleteById(Long id) {
		Optional<BasicProject> basicProject = this.findById(id);
		if (basicProject.isPresent()) {
			if (!CollectionUtils.isEmpty(basicProject.get().getBasicProjectItems())) {
				basicProjectItemRepository.delete(basicProject.get().getBasicProjectItems());
			}
			basicProjectRepository.delete(id);
			return true;
		}
		return false;
	}

	// TODO nicolas
	private BasicProject getByIdOrInstantiateNew(BasicProject basicProject) {
		return this.findById(basicProject.getId()).orElse(new BasicProject());
	}

	// TODO nicolas
	private BasicProject getAndUpdateFetchedObject(User user, BasicProject basicProject) {
		BasicProject fetchedObject = this.getByIdOrInstantiateNew(basicProject);
		if (!fetchedObject.isTypeSelectionDisabled())
			fetchedObject.setSolicitationType(basicProject.getSolicitationType());
		fetchedObject.setUnity(basicProject.getUnity());
		fetchedObject.setJustification(basicProject.getJustification());
		fetchedObject.setObject(basicProject.getObject());
		fetchedObject.setRequester(user);
		if (fetchedObject.getStatus() == null) {
			fetchedObject.setStatus(ProjectStatus.DRAFT);
		}
		if (fetchedObject.getCreatedDate() == null) {
			fetchedObject.setCreatedDate(LocalDateTime.now());
		}
		return fetchedObject;
	}

	// TODO RNG008 nicolas
	public Long saveBasicProject(BindingResult bindingResult, User user, BasicProject basicProject) {
		if (bindingResult.hasErrors() || basicProject == null)
			return null;
		BasicProject fetchedObject = this.getAndUpdateFetchedObject(user, basicProject);

		// TODO RNG008 nicolas
		if (fetchedObject.isChangesDisabled()) {
			bindingResult.addError(new ObjectError("basicProject", messages.get("basicProject.alreadyReleased")));
			return null;
		}
		return basicProjectRepository.save(fetchedObject).getId();
	}

	// TODO RNG006 / RNG007 nicolas
	public BasicProject releaseForEvaluation(BindingResult bindingResult, User user, BasicProject basicProject) {
		BasicProject fetchedObject = this.getAndUpdateFetchedObject(user, basicProject);
		// TODO RNG006 nicolas
		if (CollectionUtils.isEmpty(fetchedObject.getBasicProjectItems())) {
			bindingResult
					.addError(new ObjectError("basicProject", messages.get("basicProject.atLeastOneItemNecessary")));
		}

		if (bindingResult.hasErrors())
			return basicProject;

		// TODO RNG007 nicolas
		fetchedObject.setYear(LocalDate.now().getYear());
		fetchedObject
				.setProcessNumberInYear(basicProjectRepository.getMaxProcessNumberInYear(fetchedObject.getYear()) + 1);
		fetchedObject.setStatus(ProjectStatus.QUANTITY_EVALUATION);
		fetchedObject.setReleasedDateToQuantitiesEvaluation(LocalDateTime.now());

		return basicProjectRepository.save(fetchedObject);
	}

	// TODO nicolas RNG016 RNG020 RNG021
	public BasicProject releaseForFinancialEvaluation(BindingResult bindingResult, User user, Long id) {
		BasicProject basicProject = this.findById(id).orElse(null);
		if (basicProject == null) {
			bindingResult.addError(new ObjectError("basicProject", messages.get("record.notFound")));
			return null;
		} else {
			boolean atLeastOneAproved = false;
			ArrayList<Long> notEvaluated = new ArrayList<>();
			for (BasicProjectItem item : basicProject.getBasicProjectItems()) {
				if (item.getApproved() == null) {
					notEvaluated.add(item.getId());
					// TODO RNG020
					item.setHasError(true);
				} else if (item.getApproved()) {
					atLeastOneAproved = true;
				}
			}
			// TODO RNG016
			if (notEvaluated.isEmpty()) {
				basicProject.setQuantityEvaluator(user);
				basicProject.setReleasedDateToFinancialEvaluation(LocalDateTime.now());
				if (atLeastOneAproved) {
					basicProject.setStatus(ProjectStatus.WAITING_FINANCIAL_HEADING);
				} else {
					// TODO RNG021
					basicProject.setStatus(ProjectStatus.CANCELED);
				}
				basicProject = basicProjectRepository.save(basicProject);
			} else {
				bindingResult.addError(
						new ObjectError("basicProject", messages.get("evaluateQuantities.allItemsMustHaveStatus")));
				return null;
			}
		}
		return basicProject;
	}

	// TODO nicolas
	public List<Object[]> findBasicProjectsCounters(User user) {
		return basicProjectRepository.findBasicProjectsCounters(user.getId());
	}

	public BasicProject approveForBidding(List<String> errors, User user, Long id) {
		BasicProject basicProject = this.findById(id).orElse(null);
		if (basicProject == null) {
			errors.add(messages.get("record.notFound"));
			return null;
		} else {
			ArrayList<Long> notHeading = new ArrayList<>();
			for (BasicProjectItem item : basicProject.getApprovedBasicProjectItems()) {
				if (item.getFinancialHeading() == null) {
					notHeading.add(item.getId());
					item.setHasError(true);
				}
			}
			if (notHeading.isEmpty()) {
				basicProject.setFinancialHeadingsEvaluator(user);
				basicProject.setStatus(ProjectStatus.APPROVED_FOR_BIDDING);
				basicProject.setApprovedDateForBiddings(LocalDateTime.now());
				basicProject = basicProjectRepository.save(basicProject);
			} else {
				errors.add(messages.get("financialHeadings.allItemsMustHaveHeading"));
			}
		}
		return basicProject;
	}

	public BasicProject returnToQuantitiesEvaluation(User user, BasicProject basicProject) {
		BasicProject fetchedbasicProject = this.findById(basicProject.getId()).orElse(null);
		if (fetchedbasicProject == null) {
			return null;
		}
		fetchedbasicProject.setFinancialHeadingsEvaluator(user);
		fetchedbasicProject.setStatus(ProjectStatus.QUANTITIES_REVALUATION);
		fetchedbasicProject.setReturnReason(basicProject.getReturnReason());
		return basicProjectRepository.save(fetchedbasicProject);
	}
}
