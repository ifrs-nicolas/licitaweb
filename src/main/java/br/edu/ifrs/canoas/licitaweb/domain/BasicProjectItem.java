package br.edu.ifrs.canoas.licitaweb.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BasicProjectItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private BasicProject basicProject; // projeto básico pai do item

	@ManyToOne
	private MeasurementUnity measurementUnity; // unidade de medida do item, ex: metro, litro, quilo

	@NotNull
	@Positive
	private Integer itemNumber; // se produto será o código do produto no sistema SIGH

	@NotBlank
	@Size(max = 250)
	private String name; // se for produto o nome virá da tabela de produtos do sistema SIGH

	@NotBlank
	@Size(max = 250)
	private String description; // se for produto a descrição virá da tabela de produtos do sistema SIGH

	//TODO RNG0010 nicolas
	@NotNull
	@Positive
	private Double quantity; // quantidade solicitada

	@Positive
	private Double quantityApproved; // quantidade aprovada pelo chefe dos almox

	private Boolean approved;

	private String financialHeading; // rubrica financeira, deve estar preenchida para poder licitar o item

	@Transient
	private boolean selected; // seleção do item na tabela para possivel exclusão

	@Transient
	private SighProduct sighProduct;

	@Transient
	private Boolean hasError;

	@Transient
	public BasicProjectType getSolicitationType() {
		return basicProject == null ? null : basicProject.getSolicitationType();
	}
}
