package br.edu.ifrs.canoas.licitaweb.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.edu.ifrs.canoas.licitaweb.domain.Unity;
import br.edu.ifrs.canoas.licitaweb.repository.UnityRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UnityService {

	private final UnityRepository unityRepository;

	// TODO nicolas
	public List<Unity> findAll() {
		return unityRepository.findAll();
	}

}
