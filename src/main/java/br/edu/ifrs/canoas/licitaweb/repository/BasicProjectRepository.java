package br.edu.ifrs.canoas.licitaweb.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;

@Repository
public interface BasicProjectRepository extends JpaRepository<BasicProject, Long>, JpaSpecificationExecutor<BasicProject> {

	// TODO RNG007 nicolas
	@Query(value="SELECT COALESCE(MAX(processNumberInYear), 0) FROM BasicProject WHERE year = (:year)")
	int getMaxProcessNumberInYear(@Param("year") Integer year);

	// TODO RNG0011 nicolas
	Optional<BasicProject> findByIdAndRequesterId(Long id, Long requesterId);

	@Query(value="SELECT status, COUNT(id) FROM BasicProject WHERE requester.id = (:userId) GROUP BY status ORDER BY 2 ASC")
	List<Object[]> findBasicProjectsCounters(@Param("userId") Long userId);

}