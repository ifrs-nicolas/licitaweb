package br.edu.ifrs.canoas.licitaweb.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
public class ObjectType {

	@Id
	@GeneratedValue
	private Integer id;
	@NotBlank
	@Size(max = 100)
	private String description;
}
