package br.edu.ifrs.canoas.licitaweb.domain;

public enum BasicProjectType {
	PRODUCTS, SERVICES;
}
