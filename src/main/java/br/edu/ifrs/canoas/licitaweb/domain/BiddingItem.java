package br.edu.ifrs.canoas.licitaweb.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Data
public class BiddingItem {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne
	private BasicProjectItem basicProjectItem;

	// Campos apenas para obras de engenharia
	@ManyToOne
	private ReferenceSource referenceSource;
	private Integer referenceCode;
	private LocalDate referenceDate;
	private Double bdi;
	private Double socialCharges;

	// Demais campos
	@ManyToOne
	private MeasurementUnity unity;
	private Double unitPrice; // preço unitário devera ser preenchido apenas pelo pregoeiro
	@Transient
	private Double totalPrice;
	@ManyToOne
	private Subfamily subfamily;
	@ManyToOne
	private BudgetType budgetType; // devera ser preenchido apenas pelo pregoeiro
}
