package br.edu.ifrs.canoas.licitaweb.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.config.auth.UserImpl;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.dto.PageWrapper;
import br.edu.ifrs.canoas.licitaweb.filter.BasicProjectFilter;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectItemService;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectService;
import br.edu.ifrs.canoas.licitaweb.service.SighProductService;
import br.edu.ifrs.canoas.licitaweb.service.UnityService;
import lombok.AllArgsConstructor;

@RequestMapping("/basic-project")
@Controller
@AllArgsConstructor
public class BasicProjectController {

	private final BasicProjectService basicProjectService;
	private final BasicProjectItemService basicProjectItemService;
	private final UnityService unityService;
	private final SighProductService sighProductService;
	private final Messages messages;

	private final int[] pageOptions = { 5, 10, 20, 100 };

	// TODO RNG011 nicolas
	@GetMapping(value = { "", "/" })
	public ModelAndView listPage(@AuthenticationPrincipal UserImpl activeUser,
			@PageableDefault(page = 0, value = 5, sort = "createdDate", direction = Direction.DESC) Pageable pageable,
			BasicProjectFilter filter) {
		ModelAndView mav = new ModelAndView("/basic-project/list");
		mav.addObject("page", new PageWrapper<>("basic-project",
				basicProjectService.findAllByUser(filter, pageable, activeUser.getUser()), pageOptions));
		mav.addObject("filter", filter);
		return mav;
	}

	// TODO nicolas
	@PostMapping("/delete/{id}")
	public ModelAndView deleteBasicProject(@AuthenticationPrincipal UserImpl activeUser,
			@PathVariable("id") Long basicProjectId,
			@PageableDefault(page = 0, value = 5, sort = "createdDate", direction = Direction.DESC) Pageable pageable,
			BasicProjectFilter filter) {
		boolean deleted = basicProjectService.deleteById(basicProjectId);
		ModelAndView mav = listPage(activeUser, pageable, filter);
		mav.setViewName("/basic-project/list :: paginatedList");
		if (deleted) {
			mav.addObject("listMessage", messages.get("basicProject.deleted"));
		}
		return mav;
	}

	// TODO nicolas
	@GetMapping(value = { "/new", "/save" })
	public ModelAndView formPage() {
		ModelAndView mav = new ModelAndView("/basic-project/form");
		mav.addObject("basicProject", new BasicProject());
		mav.addObject("basicProjectItem", new BasicProjectItem());
		mav.addObject("units", unityService.findAll());
		return mav;
	}

	// TODO RNG011 nicolas
	@GetMapping("/{id}")
	public ModelAndView formEditPage(@AuthenticationPrincipal UserImpl activeUser, @PathVariable Long id) {
		Optional<BasicProject> basicProject = basicProjectService.findByIdAndRequester(id, activeUser.getUser());
		if (basicProject.isPresent()) {
			ModelAndView mav = formPage();
			mav.addObject("basicProject", basicProject.get());
			return mav;
		}
		return new ModelAndView("redirect:/basic-project/new");
	}

	// TODO RNG008 nicolas
	@Secured("ROLE_USER")
	@PostMapping("/save")
	public ModelAndView save(@AuthenticationPrincipal UserImpl activeUser, @Valid BasicProject basicProject,
			BindingResult bindingResult, RedirectAttributes redirectAttr) {
		Long id = basicProjectService.saveBasicProject(bindingResult, activeUser.getUser(), basicProject);
		if (id == null) {
			return this.formPage().addObject("basicProject", basicProject);
		}
		ModelAndView mav = new ModelAndView("redirect:/basic-project/" + id);
		redirectAttr.addFlashAttribute("message", messages.get("basicProject.saved"));
		return mav;
	}

	// TODO RNG006 / RNG007 nicolas
	@Secured("ROLE_USER")
	@PostMapping(path = "/save", params = "action=release")
	public ModelAndView releaseForEvaluation(@AuthenticationPrincipal UserImpl activeUser,
			@Valid BasicProject basicProject, BindingResult bindingResult, RedirectAttributes redirectAttr) {
		basicProject = basicProjectService.releaseForEvaluation(bindingResult, activeUser.getUser(), basicProject);
		if (bindingResult.hasErrors())
			return this.formPage().addObject("basicProject", basicProject);
		ModelAndView mav = new ModelAndView("redirect:/basic-project/" + basicProject.getId());
		redirectAttr.addFlashAttribute("message", messages.get("basicProject.released"));
		return mav;
	}

	// TODO RNG003 nicolas
	@GetMapping("/{id}/item/new")
	public ModelAndView newItem(@AuthenticationPrincipal UserImpl activeUser, @PathVariable("id") Long basicProjectId) {
		BasicProjectItem item = new BasicProjectItem();
		item.setBasicProject(basicProjectService.findById(basicProjectId).orElse(null));
		ModelAndView mav = this.formEditPage(activeUser, basicProjectId);
		mav.setViewName("/basic-project/form :: itemModalFragment");
		mav.addObject("products", sighProductService.findAll());
		mav.addObject("basicProjectItem", item);
		return mav;
	}

	// TODO RNG003 nicolas
	@GetMapping("/{id}/item/{itemId}")
	public ModelAndView editItem(@AuthenticationPrincipal UserImpl activeUser, @PathVariable("id") Long basicProjectId,
			@PathVariable("itemId") Long itemId) {
		ModelAndView mav = newItem(activeUser, basicProjectId);
		BasicProjectItem item = basicProjectItemService.findById(itemId).orElse(new BasicProjectItem());
		if (BasicProjectType.PRODUCTS.equals(item.getSolicitationType()) && item.getItemNumber() != null) {
			item.setSighProduct(sighProductService.findById(item.getItemNumber()));
		}
		return mav.addObject("basicProjectItem", item);
	}

	// TODO RNG004 nicolas
	@PostMapping("/{basicProjectId}/item/save")
	public ModelAndView saveItem(@AuthenticationPrincipal UserImpl activeUser,
			@PathVariable("basicProjectId") Long basicProjectId, @Valid BasicProjectItem basicProjectItem,
			BindingResult bindingResult, RedirectAttributes redirectAttr) {
		boolean addNewMode = (basicProjectItem.getId() == null);
		basicProjectItem = basicProjectItemService.addOrUpdateItem(basicProjectId, basicProjectItem, bindingResult);
		if (bindingResult.hasErrors())
			return this.newItem(activeUser, basicProjectId).addObject("basicProjectItem", basicProjectItem)
					.addObject(BindingResult.MODEL_KEY_PREFIX + "basicProjectItem", bindingResult);
		if (addNewMode) {
			return this.newItem(activeUser, basicProjectId).addObject("modalMessage",
					messages.get("bpItem.addedSuccessful"));
		}
		return this.editItem(activeUser, basicProjectId, basicProjectItem.getId()).addObject("modalMessage",
				messages.get("bpItem.editedSuccessful"));
	}

	// TODO nicolas
	@PostMapping("/{id}/item/delete-items")
	public ModelAndView deleteItems(@PathVariable("id") Long basicProjectId, BasicProject basicProject,
			BindingResult bindingResult, RedirectAttributes redirectAttr) {
		basicProjectItemService.deleteItems(basicProject);
		ModelAndView mav = new ModelAndView("redirect:/basic-project/" + basicProjectId);
		return mav;
	}

	// TODO RNG002 nicolas
	@GetMapping("/{id}/refresh-items-table")
	public ModelAndView refreshItemsTable(@AuthenticationPrincipal UserImpl activeUser,
			@PathVariable("id") Long basicProjectId) {
		ModelAndView mav = this.formEditPage(activeUser, basicProjectId);
		mav.setViewName("/basic-project/form :: projectItemsFragment");
		return mav;
	}
}
