package br.edu.ifrs.canoas.licitaweb.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.config.auth.UserImpl;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;
import br.edu.ifrs.canoas.licitaweb.dto.PageWrapper;
import br.edu.ifrs.canoas.licitaweb.filter.BasicProjectFilter;
import br.edu.ifrs.canoas.licitaweb.report.BasicProjectItemsExcelView;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectItemService;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectService;
import br.edu.ifrs.canoas.licitaweb.service.UnityService;
import lombok.AllArgsConstructor;

@Secured({"ROLE_ADMIN", "ROLE_CHEFE_ALMOXARIFADOS"})
@RequestMapping("/evaluate-quantities")
@Controller
@AllArgsConstructor
public class EvaluateQuantitiesController {

	private final Messages messages;
	private final BasicProjectService basicProjectService;
	private final BasicProjectItemService basicProjectItemService;
	private final UnityService unityService;
	private final BasicProjectItemsExcelView excelView;

	private final int[] pageOptions = { 5, 10, 20, 100 };

	// TODO nicolas
	@GetMapping(value = { "", "/", "/release" })
	public ModelAndView listPage(@AuthenticationPrincipal UserImpl activeUser,
			@PageableDefault(page = 0, value = 5, sort = "releasedDateToQuantitiesEvaluation", direction = Direction.ASC) Pageable pageable,
			BasicProjectFilter filter) {
		ModelAndView mav = new ModelAndView("/evaluate-quantities/list");
		mav.addObject("page", new PageWrapper<>("evaluate-quantities",
				basicProjectService.findToQuantitiesEvaluation(filter, pageable), pageOptions));
		mav.addObject("filter", filter);
		return mav;
	}

	// TODO nicolas
	private ModelAndView formPage() {
		ModelAndView mav = new ModelAndView("/evaluate-quantities/form");
		mav.addObject("basicProject", new BasicProject());
		mav.addObject("basicProjectItem", new BasicProjectItem());
		mav.addObject("units", unityService.findAll());
		return mav;
	}

	// TODO nicolas
	@GetMapping("/{id}")
	public ModelAndView formEditPage(@AuthenticationPrincipal UserImpl activeUser, @PathVariable Long id) {
		BasicProject basicProject = basicProjectService.findByIdToQuantitiesEvaluation(id);
		if (basicProject != null) {
			ModelAndView mav = formPage();
			mav.addObject("basicProject", basicProject);
			return mav;
		}
		return new ModelAndView("redirect:/evaluate-quantities");
	}

	// TODO nicolas RNG016 RNG020 RNG021
	@PostMapping(path = "/release")
	public ModelAndView releaseForFinancialEvaluation(@AuthenticationPrincipal UserImpl activeUser,
			BasicProject basicProject, BindingResult bindingResult, RedirectAttributes redirectAttr) {
		Long id = basicProject.getId();
		BasicProject updatedBP = basicProjectService.releaseForFinancialEvaluation(bindingResult, activeUser.getUser(), id);
		if (updatedBP == null)
			return this.formPage()
					.addObject("basicProject", basicProjectService.findById(id).orElse(new BasicProject()))
					.addObject(BindingResult.MODEL_KEY_PREFIX + "basicProject", bindingResult);
		ModelAndView mav = new ModelAndView("redirect:/evaluate-quantities/" + updatedBP.getId());
		String msg = ProjectStatus.WAITING_FINANCIAL_HEADING.equals(updatedBP.getStatus())
				? messages.get("evaluateQuantities.releasedToFinancialEvaluation")
				: messages.get("evaluateQuantities.canceled");
		redirectAttr.addFlashAttribute("message", msg);
		return mav;
	}

	// TODO nicolas RNG022
	@PostMapping("/edit-quantity")
	public ModelAndView editItemQuantity(@AuthenticationPrincipal UserImpl activeUser,
			BasicProjectItem basicProjectItem) {
		ModelAndView mav = new ModelAndView("/evaluate-quantities/fragments/itemRow :: itemRow");
		mav.addObject("item", basicProjectItemService.changeQuantity(basicProjectItem));
		return mav;
	}

	// TODO nicolas RNG014 RNG018
	@PostMapping("/approve")
	public ModelAndView approveItem(@AuthenticationPrincipal UserImpl activeUser, BasicProjectItem basicProjectItem) {
		ModelAndView mav = new ModelAndView("/evaluate-quantities/fragments/itemRow :: itemRow");
		mav.addObject("item", basicProjectItemService.approveItem(basicProjectItem.getId()));
		return mav;
	}

	// TODO nicolas RNG014 RNG018
	@PostMapping("/disapprove")
	public ModelAndView disapproveItem(@AuthenticationPrincipal UserImpl activeUser,
			BasicProjectItem basicProjectItem) {
		ModelAndView mav = new ModelAndView("/evaluate-quantities/fragments/itemRow :: itemRow");
		mav.addObject("item", basicProjectItemService.disapproveItem(basicProjectItem.getId()));
		return mav;
	}

	// TODO nicolas RNG019
	@GetMapping(path = "/download")
	public ModelAndView genereteItemsReportByBasicProject(Long id) {
		BasicProject basicProject = basicProjectService.findByIdToQuantitiesEvaluation(id);
		if (basicProject == null)
			return new ModelAndView("redirect:/evaluate-quantities");
		return new ModelAndView(excelView, "basicProject", basicProject);
	}

}
