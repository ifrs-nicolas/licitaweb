package br.edu.ifrs.canoas.licitaweb.report;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.util.PoiUtils;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class BasicProjectItemsExcelView extends AbstractXlsView {

	private Messages messages;

	@Override
	protected Workbook createWorkbook(Map<String, Object> model, HttpServletRequest request) {
		try {
			ClassPathResource classPathResource = new ClassPathResource("/static/poi/basic-project.xls");
			InputStream inputStream = classPathResource.getInputStream();
			return new HSSFWorkbook(inputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new HSSFWorkbook();
	}

	// TODO nicolas RNG019
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		workbook.setMissingCellPolicy(MissingCellPolicy.CREATE_NULL_AS_BLANK);
		CreationHelper createHelper = workbook.getCreationHelper();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy hh:mm"));

		CellStyle cellStyleBold = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		cellStyleBold.setFont(font);

		CellStyle styleDisaproved = workbook.createCellStyle();
		Font fontDisaproved = workbook.createFont();
		fontDisaproved.setColor(Font.COLOR_RED);
		styleDisaproved.setFont(fontDisaproved);

		BasicProject basicProject = (BasicProject) model.get("basicProject");
		response.setHeader("Content-Disposition",
				"attachment; filename=\"" + messages.get("bpPoi.filename") + " " + basicProject.getYear() + "-"
						+ String.format("%03d", basicProject.getProcessNumberInYear()) + ".xls\"");

		Sheet sheet = workbook.getSheetAt(0);
		workbook.setSheetName(0, messages.get("bpPoi.basicProject") + " " + basicProject.getId());

		Row row = PoiUtils.getRow(sheet, 0);
		row.getCell(0).setCellValue(messages.get("bpPoi.basicProject"));
		row.getCell(1).setCellValue(basicProject.getProcessNumber());
		row.getCell(0).setCellStyle(cellStyleBold);
		row.getCell(1).setCellStyle(cellStyleBold);

		row = PoiUtils.getRow(sheet, 1);
		row.getCell(0).setCellValue(messages.get("bpPoi.requester"));
		row.getCell(1).setCellValue(basicProject.getRequester().getName());

		row = PoiUtils.getRow(sheet, 2);
		row.getCell(0).setCellValue(messages.get("bpPoi.releasedDateToQuantitiesEvaluation"));
		row.getCell(1).setCellValue(Timestamp.valueOf(basicProject.getReleasedDateToQuantitiesEvaluation()));
		row.getCell(1).setCellStyle(cellDateStyle);

		row = PoiUtils.getRow(sheet, 3);
		row.getCell(0).setCellValue(messages.get("bpPoi.solicitationType"));
		row.getCell(1).setCellValue(messages.get("bpPoi.solicitationType." + basicProject.getSolicitationType()));


		row = PoiUtils.getRow(sheet, 4);
		row.getCell(0).setCellValue(messages.get("bpPoi.unity"));
		row.getCell(1).setCellValue(basicProject.getUnity().getName());

		row = PoiUtils.getRow(sheet, 5);
		row.getCell(0).setCellValue(messages.get("bpPoi.object"));
		row.getCell(1).setCellValue(basicProject.getObject());

		row = PoiUtils.getRow(sheet, 6);
		row.getCell(0).setCellValue(messages.get("bpPoi.justification"));
		row.getCell(1).setCellValue(basicProject.getJustification());

		row = PoiUtils.getRow(sheet, 7);
		row.getCell(0).setCellValue(messages.get("bpPoi.status"));
		row.getCell(1).setCellValue(messages.get("basicProject.status." + basicProject.getStatus()));

		row = PoiUtils.getRow(sheet, 9);
		row.getCell(0).setCellValue(messages.get("bpItem.itemNumber"));
		row.getCell(1).setCellValue(messages.get("bpItem.name"));
		row.getCell(2).setCellValue(messages.get("bpItem.description"));
		row.getCell(3).setCellValue(messages.get("bpItem.requestedQuantity"));
		row.getCell(4).setCellValue(messages.get("bpItem.approvedQuantity"));
		row.getCell(5).setCellValue(messages.get("bpPoi.itemStatus"));
		row.getCell(0).setCellStyle(cellStyleBold);
		row.getCell(1).setCellStyle(cellStyleBold);
		row.getCell(2).setCellStyle(cellStyleBold);
		row.getCell(3).setCellStyle(cellStyleBold);
		row.getCell(4).setCellStyle(cellStyleBold);
		row.getCell(5).setCellStyle(cellStyleBold);

		int i = 10;
		for (BasicProjectItem item : basicProject.getBasicProjectItems()) {
			row = PoiUtils.getRow(sheet, i);
			row.getCell(0).setCellValue(PoiUtils.toPrimitive(item.getItemNumber()));
			row.getCell(1).setCellValue(item.getName());
			row.getCell(2).setCellValue(item.getDescription());
			row.getCell(3).setCellValue(PoiUtils.toPrimitive(item.getQuantity()));
			row.getCell(4).setCellValue(PoiUtils.toPrimitive(item.getQuantityApproved()));
			row.getCell(5).setCellValue(messages.get("evaluateQuantities.status." + item.getApproved()));
			if (item.getApproved() != null && !item.getApproved()) {
				row.setRowStyle(styleDisaproved);
			}
			i = i + 1;
		}
	}
}
