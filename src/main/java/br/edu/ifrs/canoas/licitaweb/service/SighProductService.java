package br.edu.ifrs.canoas.licitaweb.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.edu.ifrs.canoas.licitaweb.domain.SighProduct;
import br.edu.ifrs.canoas.licitaweb.repository.SighProductRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class SighProductService {

	private final SighProductRepository sighProductRepository;

	// TODO nicolas
	public List<SighProduct> findAll() {
		return sighProductRepository.findAll();
	}

	// TODO nicolas
	public SighProduct findById(Integer id) {
		return sighProductRepository.findOne(id);
	}
}
