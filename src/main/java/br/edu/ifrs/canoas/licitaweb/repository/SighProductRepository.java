package br.edu.ifrs.canoas.licitaweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifrs.canoas.licitaweb.domain.SighProduct;

@Repository
public interface SighProductRepository extends JpaRepository<SighProduct, Integer> {

}