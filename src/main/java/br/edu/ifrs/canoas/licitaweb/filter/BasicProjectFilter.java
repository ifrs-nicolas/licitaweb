package br.edu.ifrs.canoas.licitaweb.filter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;
import br.edu.ifrs.canoas.licitaweb.domain.User;
import br.edu.ifrs.canoas.licitaweb.util.SearchUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BasicProjectFilter {

	private String object;
	private String justification;
	private Long requesterId;
	private LocalDate releasedDateStart;
	private LocalDate releasedDateEnd;
	private List<String> notEqualStatus;

	// TODO RNG012 nicolas
	public Specification<BasicProject> toSpec() {
		BasicProjectFilter filter = this;
		return (root, query, builder) -> {
			final Collection<Predicate> predicates = new ArrayList<>();
			SearchUtils.search(predicates, builder, root, "object", filter.getObject());
			SearchUtils.search(predicates, builder, root, "justification", filter.getJustification());
			SearchUtils.search(predicates, builder, root.<User>get("requester"), "id", filter.getRequesterId());
			if (!CollectionUtils.isEmpty(filter.getNotEqualStatus())) {
				for (String status : filter.getNotEqualStatus()) {
					predicates.add(builder.notEqual(root.get("status"), ProjectStatus.valueOf(status)));
				}
			}
			SearchUtils.search(predicates, builder, root, "releasedDateToQuantitiesEvaluation", filter.getReleasedDateStart(), filter.getReleasedDateEnd());
			return builder.and(predicates.toArray(new Predicate[predicates.size()]));
		};

	}
}
