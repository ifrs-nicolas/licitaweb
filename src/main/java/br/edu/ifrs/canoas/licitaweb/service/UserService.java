package br.edu.ifrs.canoas.licitaweb.service;

import org.springframework.stereotype.Service;

import br.edu.ifrs.canoas.licitaweb.domain.User;
import br.edu.ifrs.canoas.licitaweb.repository.UserRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserService {

	private final UserRepository userRepository;

	public User save(User user) {
		User fetchedUser = this.getOne(user);
		if (fetchedUser == null)
			return null;
		fetchedUser.setName(user.getName());
		fetchedUser.setEmail(user.getEmail());
		return userRepository.save(fetchedUser);
	}

	public User getOne(User user) {
		if (user == null || user.getId() == null)
			return null;
		return userRepository.findOne(user.getId());
	}
}
