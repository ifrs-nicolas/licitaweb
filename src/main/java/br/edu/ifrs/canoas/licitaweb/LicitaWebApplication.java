package br.edu.ifrs.canoas.licitaweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LicitaWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(LicitaWebApplication.class, args);
	}
}
