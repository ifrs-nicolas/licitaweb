package br.edu.ifrs.canoas.licitaweb.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import br.edu.ifrs.canoas.licitaweb.config.Messages;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProject;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;
import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectType;
import br.edu.ifrs.canoas.licitaweb.domain.ProjectStatus;
import br.edu.ifrs.canoas.licitaweb.repository.BasicProjectItemRepository;
import br.edu.ifrs.canoas.licitaweb.repository.BasicProjectRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class BasicProjectItemService {

	private final BasicProjectItemRepository basicProjectItemRepository;
	private final BasicProjectRepository basicProjectRepository;
	private final Messages messages;

	public Optional<BasicProjectItem> findById(Long id) {
		return id == null ? Optional.empty() : Optional.ofNullable(basicProjectItemRepository.findOne(id));
	}

	// TODO RNG008 / RNG009 / RNG010 nicolas
	public BasicProjectItem addOrUpdateItem(Long basicProjectId, BasicProjectItem basicProjectItem,
			BindingResult bindingResult) {
		BasicProject basicProject = basicProjectRepository.findOne(basicProjectId);
		basicProjectItem.setBasicProject(basicProject);

		// TODO RNG008 nicolas
		if (!ProjectStatus.DRAFT.equals(basicProject.getStatus())) {
			bindingResult.addError(new ObjectError("basicProject", messages.get("basicProject.alreadyReleased")));
		}

		// TODO RNG009 nicolas
		if (BasicProjectType.PRODUCTS.equals(basicProject.getSolicitationType())) {
			if (basicProjectItem.getSighProduct() == null) {
				bindingResult
						.addError(new FieldError("basicProjectItem", "sighProduct", messages.get("field.not-null")));
			} else {
				basicProjectItem.setItemNumber(basicProjectItem.getSighProduct().getId());
				basicProjectItem.setName(basicProjectItem.getSighProduct().getName());
				basicProjectItem.setDescription(basicProjectItem.getSighProduct().getDescription());
			}
		}

		// TODO RNG010 nicolas
		Optional<BasicProjectItem> bpiAux = basicProjectItemRepository.findByBasicProjectIdAndItemNumber(basicProjectId,
				basicProjectItem.getItemNumber());
		if (bpiAux.isPresent() && !bpiAux.get().getId().equals(basicProjectItem.getId())) {
			bindingResult.addError(
					new ObjectError("basicProject", messages.get("basicProject.notPossibleSameItemCodeTwice")));
		}

		if (bindingResult.hasErrors())
			return basicProjectItem;
		return basicProjectItemRepository.save(basicProjectItem);
	}

	// TODO nicolas
	public void deleteItems(BasicProject basicProject) {
		if (basicProject != null && !basicProject.getBasicProjectItems().isEmpty()) {
			ArrayList<BasicProjectItem> itensToDelete = new ArrayList<>();
			basicProject.getBasicProjectItems().forEach(item -> {
				if (item.isSelected()) {
					BasicProjectItem bpi = basicProjectItemRepository.findOne(item.getId());
					if (bpi != null) {
						itensToDelete.add(bpi);
					}
				}
			});
			basicProjectItemRepository.delete(itensToDelete);
		}
	}

	// TODO nicolas RNG022
	public BasicProjectItem changeQuantity(BasicProjectItem basicProjectItem) {
		BasicProjectItem item = this.findById(basicProjectItem.getId()).orElse(null);
		if (item != null) {
			item.setQuantityApproved(basicProjectItem.getQuantityApproved());
			item.setApproved(null);
			item = basicProjectItemRepository.save(item);
		}
		return item;
	}

	// TODO nicolas RNG014 RNG018
	public BasicProjectItem approveItem(Long id) {
		BasicProjectItem item = this.findById(id).orElse(null);
		if (item != null) {
			if (item.getQuantityApproved() == null) {
				item.setQuantityApproved(item.getQuantity());
			}
			item.setApproved(true);
			item = basicProjectItemRepository.save(item);
		}
		return item;
	}

	// TODO nicolas RNG014 RNG018
	public BasicProjectItem disapproveItem(Long id) {
		BasicProjectItem item = this.findById(id).orElse(null);
		if (item != null) {
			item.setApproved(false);
			item = basicProjectItemRepository.save(item);
		}
		return item;
	}

	public BasicProjectItem changeFinancialHeading(BasicProjectItem bpItem) {
		Optional<BasicProjectItem> itemOptional = this.findById(bpItem.getId());
		if (itemOptional.isPresent()) {
			BasicProjectItem item = itemOptional.get();
			item.setFinancialHeading(
					StringUtils.hasText(bpItem.getFinancialHeading()) ? bpItem.getFinancialHeading().trim() : null);
			return basicProjectItemRepository.save(item);
		};
		return null;
	}
}
