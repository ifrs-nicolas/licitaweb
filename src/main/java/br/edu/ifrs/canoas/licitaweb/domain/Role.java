package br.edu.ifrs.canoas.licitaweb.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by rodrigo on 3/18/17.
 */
@Entity
@Data
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String role;
	@ManyToMany
	private Set<User> accounts;
}
