package br.edu.ifrs.canoas.licitaweb.dto;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import br.edu.ifrs.canoas.licitaweb.util.PageWrapperUtils;
import lombok.Data;

@Data
public class PageWrapper<T> {
	public static final int MAX_PAGINATIONBAR_SIZE = 5;

	private List<T> content;
	private int size;
	private int page;
	private int number;
	private long totalElements;
	private int totalPages;
	private boolean first;
	private boolean last;
	private boolean previous;
	private boolean next;
	private Sort sort;

	private String url;
	private int[] pageOptions = { 5, 10, 20, 100 };
	private int paginationbarStart;
	private int paginationbarEnd;
	private int paginationbarSize;

	public PageWrapper(String url, Page<T> page) {
		PageWrapperUtils.updateWrapper(this, url, page, pageOptions);
	}

	public PageWrapper(String url, Page<T> page, int[] pageOptions) {
		PageWrapperUtils.updateWrapper(this, url, page, pageOptions);
	}

	public String getSortDir(String property) {
		if (this.sort != null && this.sort.getOrderFor(property) != null)
			return this.sort.getOrderFor(property).getDirection().name();
		return null;
	}
}
