package br.edu.ifrs.canoas.licitaweb.util;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class PoiUtils {

	private PoiUtils() {
	}

	public static Row getRow(Sheet sheet, int rownum) {
		Row row = sheet.getRow(rownum);
		return row == null ? sheet.createRow(rownum) : row;
	}

	public static int toPrimitive(Integer value) {
		return value == null ? 0 : value.intValue();
	}

	public static double toPrimitive(Double value) {
		return value == null ? 0 : value.doubleValue();
	}
}
