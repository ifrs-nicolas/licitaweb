package br.edu.ifrs.canoas.licitaweb.util;

import org.springframework.data.domain.Sort;

public class PageRequest {

	public static org.springframework.data.domain.PageRequest of(int page, int size) {
		return new org.springframework.data.domain.PageRequest(page, size);
	}

	public static org.springframework.data.domain.PageRequest of(int page, int size, Sort sort) {
		return new org.springframework.data.domain.PageRequest(page, size, sort);
	}
}
