package br.edu.ifrs.canoas.licitaweb.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifrs.canoas.licitaweb.config.auth.UserImpl;
import br.edu.ifrs.canoas.licitaweb.service.BasicProjectService;
import lombok.AllArgsConstructor;

/**
 * Created by nicolas on 4/13/18.
 */
@Controller
@AllArgsConstructor
public class HomeController {

	private final BasicProjectService basicProjectService;

	// TODO nicolas
	@GetMapping("/")
	public ModelAndView publicIndex() {
		return new ModelAndView("/index");
	}

	// TODO nicolas private landing page
	@GetMapping("/home")
	public ModelAndView home(@AuthenticationPrincipal UserImpl activeUser) {
		ModelAndView mav =  new ModelAndView("/home");
		mav.addObject("basicProjectsCounters", basicProjectService.findBasicProjectsCounters(activeUser.getUser()));
		return mav;
	}
}
