package br.edu.ifrs.canoas.licitaweb.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
public class Family {

	@Id
	@GeneratedValue
	private Integer id;
	private Integer code;
	@NotBlank
	@Size(max = 250)
	private String description;
}
