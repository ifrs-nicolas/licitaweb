package br.edu.ifrs.canoas.licitaweb.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(exclude = "basicProjectItems")
@ToString(exclude = "basicProjectItems")
public class BasicProject {

	@Id
	@GeneratedValue
	private Long id;
	@OneToMany(mappedBy = "basicProject")
	private List<BasicProjectItem> basicProjectItems;
	private LocalDateTime createdDate;
	private LocalDateTime releasedDateToQuantitiesEvaluation;
	private LocalDateTime releasedDateToFinancialEvaluation;
	private LocalDateTime approvedDateForBiddings;
	private Integer year;
	private Integer processNumberInYear;
	@Enumerated(EnumType.ORDINAL)
	private ProjectStatus status;
	@NotNull
	@ManyToOne
	private Unity unity;
	@ManyToOne
	private User requester;
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	private BasicProjectType solicitationType;
	@NotBlank
	@Size(max = 100)
	private String object;
	@NotBlank
	@Size(max = 300)
	private String justification;
	@ManyToOne
	private User quantityEvaluator;
	@ManyToOne
	private User financialHeadingsEvaluator;
	@Size(max = 300)
	private String returnReason;

	// TODO RNG007 nicolas
	@Transient
	public String getProcessNumber() {
		return (processNumberInYear == null || year == null) ? "/"
				: String.format("%03d", processNumberInYear) + "/" + year;
	}

	// TODO RNG008 nicolas
	@Transient
	public Boolean isChangesDisabled() {
		return this.getStatus() != null && !ProjectStatus.DRAFT.equals(this.getStatus());
	}

	@Transient
	public Boolean isQuantitiesEvaluationDisabled() {
		return this.getStatus() == null || (!ProjectStatus.QUANTITY_EVALUATION.equals(this.getStatus())
				&& !ProjectStatus.QUANTITIES_REVALUATION.equals(this.getStatus()));
	}

	@Transient
	public Boolean isFinancialEvaluationDisabled() {
		return this.getStatus() == null || (!ProjectStatus.WAITING_FINANCIAL_HEADING.equals(this.getStatus()));
	}

	@Transient
	public Boolean isTypeSelectionDisabled() {
		return this.getId() != null && this.getSolicitationType() != null;
	}

	@Transient
	public List<BasicProjectItem> getApprovedBasicProjectItems() {
		return (this.basicProjectItems == null) ? null
				: this.basicProjectItems.stream().filter(item -> Boolean.TRUE.equals(item.getApproved()))
						.collect(Collectors.toList());
	}
}
