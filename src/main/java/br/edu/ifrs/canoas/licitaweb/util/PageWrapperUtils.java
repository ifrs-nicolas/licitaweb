package br.edu.ifrs.canoas.licitaweb.util;

import org.springframework.data.domain.Page;

import br.edu.ifrs.canoas.licitaweb.dto.PageWrapper;

public class PageWrapperUtils {

	public static final int MAX_PAGINATIONBAR_SIZE = 5;

	private PageWrapperUtils() {}

	public static <T> void updateWrapper(PageWrapper<T> wrapper, String url, Page<T> page, int[] pageOptions) {
		wrapper.setUrl(url);
		if (page != null) {
			wrapper.setContent(page.getContent());
			wrapper.setSize(page.getSize());
			wrapper.setNumber(page.getNumber());
			wrapper.setPage(page.getNumber()); // same as page number
			wrapper.setTotalElements(page.getTotalElements());
			wrapper.setTotalPages(page.getTotalPages());
			wrapper.setFirst(page.isFirst());
			wrapper.setLast(page.isLast());
			wrapper.setPrevious(page.hasPrevious());
			wrapper.setNext(page.hasNext());
			wrapper.setSort(page.getSort());
			int start, maxsize;
			if (page.getTotalPages() <= MAX_PAGINATIONBAR_SIZE) {
				start = 1;
				maxsize = page.getTotalPages();
			} else {
				if (page.getNumber() <= MAX_PAGINATIONBAR_SIZE - MAX_PAGINATIONBAR_SIZE / 2) {
					start = 1;
					maxsize = MAX_PAGINATIONBAR_SIZE;
				} else if (page.getNumber() >= page.getTotalPages() - MAX_PAGINATIONBAR_SIZE / 2) {
					start = page.getTotalPages() - MAX_PAGINATIONBAR_SIZE + 1;
					maxsize = MAX_PAGINATIONBAR_SIZE;
				} else {
					start = page.getNumber() + 1 - MAX_PAGINATIONBAR_SIZE / 2;
					maxsize = MAX_PAGINATIONBAR_SIZE;
				}
			}
			if (maxsize == 0) {
				maxsize = 1;
			}
			wrapper.setPaginationbarStart(start);
			wrapper.setPaginationbarEnd(start + maxsize - 1);
			wrapper.setPaginationbarSize(maxsize);
		}
		if (pageOptions != null) {
			wrapper.setPageOptions(pageOptions);
		}
	}
}
