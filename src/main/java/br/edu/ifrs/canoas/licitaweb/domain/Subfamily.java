package br.edu.ifrs.canoas.licitaweb.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
public class Subfamily {

	@Id
	@GeneratedValue
	private Integer id;
	private Integer code;
	@NotBlank
	@Size(max = 255)
	private String description;
	@ManyToOne
	private Family family;
}
