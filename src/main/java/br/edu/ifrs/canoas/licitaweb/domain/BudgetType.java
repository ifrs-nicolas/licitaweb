package br.edu.ifrs.canoas.licitaweb.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
public class BudgetType {

	@Id
	@GeneratedValue
	private Integer id;
	@NotBlank
	@Size(max = 50)
	private String description;
}
