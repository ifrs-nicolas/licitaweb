package br.edu.ifrs.canoas.licitaweb.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifrs.canoas.licitaweb.domain.BasicProjectItem;

@Repository
public interface BasicProjectItemRepository extends JpaRepository<BasicProjectItem, Long> {

	// TODO RNG010 nicolas
	Optional<BasicProjectItem> findByBasicProjectIdAndItemNumber(Long basicProjectId, Integer itemNumber);

}