package br.edu.ifrs.canoas.licitaweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifrs.canoas.licitaweb.domain.Unity;

@Repository
public interface UnityRepository extends JpaRepository<Unity, Integer> {

}