// TODO nicolas RNG017
function editQuantity(itemId) {
	$('#span-heading-' + itemId).addClass('hiddenMode');
	$('#input-heading-' + itemId).removeClass('hiddenMode');
	$('#common-mode-' + itemId).addClass('hiddenMode');
	$('#edit-mode-' + itemId).removeClass('hiddenMode');
}

function editQuantityCancel(itemId) {
	$('#span-heading-' + itemId).removeClass('hiddenMode');
	$('#input-heading-' + itemId).addClass('hiddenMode');
	$('#common-mode-' + itemId).removeClass('hiddenMode');
	$('#edit-mode-' + itemId).addClass('hiddenMode');
}

function editHeadingOk(itemId) {
	var url = contextRoot + "financial-headings/change-financial-heading";
	var heading = $('#input-heading-' + itemId).val()
	if (heading == "")
		heading = null;
	var itemData = {
		id : itemId,
		financialHeading : heading
	}
	$.post(url, itemData, function(response) {
		$("#itemsTableHolder-" + itemId).replaceWith(response);
	}).fail(function(error) {
		var result = handleRestError(error);
		if (result)
			alert(result);
	})
}
