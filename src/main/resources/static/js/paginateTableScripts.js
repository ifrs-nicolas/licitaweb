$(document).ready(function() {
	var scroll = Cookies.get('scroll');
	if (scroll !== null) {
		$(document).scrollTop(scroll);
		Cookies.remove('scroll');
	}
})

function refreshPage(newUrl) {
	Cookies.set('scroll', $(document).scrollTop());
	window.location.href = newUrl ? newUrl : new URI();
}

function changePage(newPageNumber) {
	var uri = new URI();
	uri.search(function(data) {
		data.page = newPageNumber;
	});
	refreshPage(uri.toString());
}

function changeSort(newSort, currentOrderBy) {
	var uri = new URI();
	uri.search(function(data) {
		if (data.sort && contains(data.sort, newSort)) {
			data.sort = newSort
				+ (contains(data.sort, 'desc') ? ",asc" : ",desc");
		} else {
			if (currentOrderBy && currentOrderBy != 'null') {
				data.sort = newSort
					+ (contains(currentOrderBy, 'DESC') ? ",asc" : ",desc");
			} else {
				data.sort = newSort + ",asc";
			}
		}
	});
	refreshPage(uri.toString());
}

function changePageSize() {
	var newPageSize = $("#page-size-selector option:selected")[0].value;
	var uri = new URI();
	uri.search(function(data) {
		data.page = 0;
		data.size = newPageSize;
	});
	refreshPage(uri.toString());
}

function cleanFilters() {
	var uri = new URI();
	uri.search(function(data) {
		return {
			size : data.size,
			page : 0
		}
	});
	refreshPage(uri.toString());
}