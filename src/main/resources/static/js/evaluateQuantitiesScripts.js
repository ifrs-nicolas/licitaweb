// TODO nicolas RNG017
function editQuantity(itemId) {
	$('#span-evaluate-' + itemId).addClass('hiddenMode');
	$('#input-evaluate-' + itemId).removeClass('hiddenMode');
	$('#common-mode-' + itemId).addClass('hiddenMode');
	$('#edit-mode-' + itemId).removeClass('hiddenMode');
	if ($('#input-evaluate-' + itemId).val() == "") {
		$('#input-evaluate-' + itemId).val($('#quantity-' + itemId).text())
	}
}

function editQuantityCancel(itemId) {
	$('#span-evaluate-' + itemId).removeClass('hiddenMode');
	$('#input-evaluate-' + itemId).addClass('hiddenMode');
	$('#common-mode-' + itemId).removeClass('hiddenMode');
	$('#edit-mode-' + itemId).addClass('hiddenMode');
}

function editQuantityOk(itemId) {
	var url = contextRoot + "evaluate-quantities/edit-quantity";
	var quantity = $('#input-evaluate-' + itemId).val()
	if (quantity <= 0)
		quantity = null;
	var itemData = {
		id : itemId,
		quantityApproved : quantity
	}
	$.post(url, itemData, function(response) {
		$("#itemsTableHolder-" + itemId).replaceWith(response);
	}).fail(function(error) {
		var result = handleRestError(error);
		if (result)
			alert(result);
	})
}

function approveItem(itemId) {
	var url = contextRoot + "evaluate-quantities/approve";
	var itemData = {
		id : itemId
	}
	$.post(url, itemData, function(response) {
		$("#itemsTableHolder-" + itemId).replaceWith(response);
	}).fail(function(error) {
		var result = handleRestError(error);
		if (result)
			alert(result);
	})
}

function disapproveItem(itemId) {
	var url = contextRoot + "evaluate-quantities/disapprove";
	var itemData = {
		id : itemId
	}
	$.post(url, itemData, function(response) {
		$("#itemsTableHolder-" + itemId).replaceWith(response);
	}).fail(function(error) {
		var result = handleRestError(error);
		if (result)
			alert(result);
	})
}
