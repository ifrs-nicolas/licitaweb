function contains(word, slice) {
	return word.indexOf(slice) != -1;
}

function saveSidebarStatus() {
	setTimeout(function() {
		Cookies.set('sidebar-hidden', $("body").hasClass("sidebar-hidden"))
	}, 500);
}

function saveSidebarStatus2() {
	setTimeout(function() {
		Cookies.set('sidebar-minimized', $("body").hasClass("sidebar-minimized"))
	}, 500);
}

$(document).ready(function() {
	var sidebarHidden = Cookies.get('sidebar-hidden');
	if (sidebarHidden == 'true') {
		$("body").addClass('sidebar-hidden');
	} else {
		$("body").removeClass('sidebar-hidden');
	}
	var sidebarMinimized = Cookies.get('sidebar-minimized');
	if (sidebarMinimized == 'true') {
		$("body").addClass('sidebar-minimized');
		$("body").addClass('brand-minimized');
	} else {
		$("body").removeClass('sidebar-minimized');
		$("body").removeClass('brand-minimized');
	}
})