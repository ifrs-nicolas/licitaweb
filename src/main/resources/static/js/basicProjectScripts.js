function viewBasicProjectItem(basicProjectId, itemId) {
	var onSuccess = function(response) {
		$('#projectItemModal').modal('hide');
		$('#projectItemHolder').html(response);
		$('#projectItemModal').modal('show');
	}
	asyncGet(contextRoot + "basic-project/" + basicProjectId + "/item/" + itemId, null,
			onSuccess);
}

//TODO RNG004 nicolas
function saveBasicProjectItem(basicProjectId, itemId) {
	$(".alert").alert('close')
	var url = contextRoot + "basic-project/" + basicProjectId + "/item/save/";
	var onSuccess = function(response) {
		$('#projectItemModal').modal('hide');
		$('#projectItemHolder').html(response);
		$('#projectItemModal').modal('show');
		asyncGet(contextRoot + "basic-project/" + basicProjectId + "/refresh-items-table",
				"#itemsTableHolder");
	}
	asyncPost(url, "#basicProjectItemForm", null, onSuccess);
}

// TODO RNG013 nicolas
function deleteById(pageUrl, basicProjectId) {
	$(".alert").alert('close')
	$('#deleteConfirmationModal').modal('show');
	$('#delete-yes').click(function (){
		var url = contextRoot + pageUrl + "/delete/" + basicProjectId;
		var onSuccess = function(response) {
			$('#deleteConfirmationModal').modal('hide');
			$('#paginatedListHolder').html(response);
		}
		asyncPost(url, "#searchFilterForm", null, onSuccess);
	})
}
