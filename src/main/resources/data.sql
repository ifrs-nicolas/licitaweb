--AUTHENTICATION
INSERT into user(username, password, name, email, active) VALUES
('user', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW', 'User','user@stars.wars', TRUE);

--ROLES
insert into role(role) values
('ROLE_USER'),
('ROLE_ADMIN'),
('ROLE_CHEFE_ALMOXARIFADOS'),
('ROLE_CONTABILIDADE'),
('ROLE_PREGOEIRO');

-- USER_ROLES
insert into user_roles (user_id, roles_id) values
(1, 1),
(1, 2);

--UNITY
insert into unity(id, name) values
(100, 'HMGV'),
(200, 'H. TRAMANDAI'),
(300, 'UPA SAPUCAIA');

--SIGH PRODUCTS
insert into sigh_product(name, description, average_price) values
('produto de teste1','produto de teste1',55),
('produto de teste2','produto de teste2',6.24),
('produto de teste3','produto de teste3',43),
('produto de teste4','produto de teste4',91.282),
('produto de teste5','produto de teste5',140),
('produto de teste6','produto de teste6',188.804),
('produto de teste7','produto de teste7',238),
('produto de teste8','produto de teste8',286.326),
('produto de teste9','produto de teste9',335),
('produto de teste10','produto de teste10',383.848),
('produto de teste11','produto de teste11',433),
('produto de teste12','produto de teste12',481.37),
('produto de teste13','produto de teste13',530),
('produto de teste14','produto de teste14',578.892),
('produto de teste15','produto de teste15',628),
('produto de teste16','produto de teste16',676.414),
('produto de teste17','produto de teste17',725),
('produto de teste18','produto de teste18',773.936),
('produto de teste19','produto de teste19',823),
('produto de teste20','produto de teste20',871.458);

--BASIC PROJECT
insert into basic_project(id, created_date, status, unity_id, requester_id, solicitation_type, object, justification, released_date_to_quantities_evaluation,
process_number_in_year, year) values
(100, '2018-05-10 12:00:00', 1, 100, 1, 0, 'Objeto de Testes 1', 'justificando o teste 1', '2018-05-10 14:00:00', 1, 2018),
(101, '2018-05-05 12:30:00', 0, 100, 1, 1, 'Objeto de Testes 2', 'justificando o teste 2', null, null, null),
(102, '2018-05-08 12:40:00', 0, 100, 1, 0, 'Objeto de Testes 3', 'justificando o teste 3', null, null, null),
(103, '2018-05-05 12:50:00', 0, 100, 1, 0, 'Objeto de Testes 4', 'justificando o teste 4', null, null, null),
(104, '2018-05-05 14:00:00', 0, 100, 1, 1, 'Objeto de Testes 5', 'justificando o teste 5', null, null, null),
(105, '2018-05-06 13:00:00', 0, 100, 1, 0, 'Objeto de Testes 6', 'justificando o teste 6', null, null, null),
(106, '2018-05-06 13:30:00', 2, 100, 1, 0, 'Objeto de Testes 7', 'justificando o teste 7', '2018-06-20 14:00:00', 3, 2018),
(107, '2018-05-06 14:00:00', 0, 100, 1, 1, 'Objeto de Testes 8', 'justificando o teste 8', null, null, null),
(108, '2018-05-06 14:30:00', 0, 100, 1, 0, 'Objeto de Testes 9', 'justificando o teste 9', null, null, null),
(109, '2018-05-07 12:00:00', 0, 100, 1, 1, 'Objeto de Testes 10', 'justificando o teste 10', null, null, null),
(110, '2018-05-07 12:30:00', 1, 100, 1, 1, 'Objeto de Testes 11', 'justificando o teste 11', '2018-06-02 14:15:00', 2, 2018),
(111, '2018-05-07 14:00:00', 0, 100, 1, 1, 'Objeto de Testes 12', 'justificando o teste 12', null, null, null),
(112, '2018-05-08 08:00:00', 0, 100, 1, 0, 'Objeto de Testes 13', 'justificando o teste 13', null, null, null),
(113, '2018-05-08 08:45:00', 0, 100, 1, 1, 'Objeto de Testes 14', 'justificando o teste 14', null, null, null);

insert into basic_project_item(approved, item_number, description, name, quantity, basic_project_id) values
(null, 1, 'produto de teste1', 'produto de teste1', 10, 100),
(null, 2, 'produto de teste2', 'produto de teste2', 8, 100),
(null, 3, 'produto de teste3', 'produto de teste3', 10, 100),
(null, 4, 'produto de teste4', 'produto de teste4', 90, 100),
(null, 5, 'produto de teste5', 'produto de teste5', 10, 100),
(null, 6, 'produto de teste6', 'produto de teste6', 242, 100);

insert into basic_project_item(approved, item_number, description, name, quantity, quantity_approved, basic_project_id) values
(true, 1, 'produto de teste1',  'produto de teste1', 010, 010, 110),
(true, 2, 'produto de teste2',  'produto de teste2', 008, 008, 110),
(true, 3, 'produto de teste3',  'produto de teste3', 010, 010, 110),
(false, 4, 'produto de teste4', 'produto de teste4', 090, 090, 110),
(true, 5, 'produto de teste5',  'produto de teste5', 010, 010, 110),
(true, 6, 'produto de teste6',  'produto de teste6', 242, 242, 110);

insert into basic_project_item(approved, item_number, description, name, quantity, quantity_approved, basic_project_id) values
(true, 1, 'produto de teste1', 'produto de teste1', 010, 010, 106),
(true, 2, 'produto de teste2', 'produto de teste2', 008, 008, 106),
(false, 3, 'produto de teste3', 'produto de teste3', 010, null, 106),
(false, 4, 'produto de teste4', 'produto de teste4', 090, null, 106),
(true, 5, 'produto de teste5', 'produto de teste5', 010, 010, 106),
(true, 6, 'produto de teste6', 'produto de teste6', 242, 242, 106);

update basic_project set released_date_to_financial_evaluation='2018-05-10 16:15:00' where id=106;
